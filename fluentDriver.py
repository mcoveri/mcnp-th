#!/usr/bin/python

#################         FLUENT DRIVER              ################
#
# Uses Fluent's journal capability to automate fluent runs.
# Creates a complete fluent journal file from a template.
# Executes Fluent using completed journal.
#
# Changelog
# ---------
#
# Author
# ------
# William Gurecky
# william.gurecky@utexas.edu
#
#####################################################################

from gmshDriver import *
import subprocess
import os
dir = os.path.dirname(os.path.abspath(__file__))


class FluentDriver(object):
    def __init__(self, templateJournalFileName, **kwargs):
        self.templateJournalFileName = templateJournalFileName
        self.runDim = kwargs.pop('dim', 2)
        self.thIters = kwargs.pop('thIters', 250)
        self.outTempData = kwargs.pop('tempData')
        self.outDensData = kwargs.pop('densityData')
        self.inVolSource = kwargs.pop('heatSource')
        self.cas = kwargs.pop('cas', os.path.join(dir, 'fluent/cases/plane_wall_combo.cas'))
        self.journalFileName = kwargs.pop('journalOut', os.path.join(dir, 'fluent/journals/run_journal.jou'))

    def writeJournalFile(self, **kwargs):
        """
        Create completed journal file from templated file.  Inject run parameters
        into template journal.
        """
        # provide defaults if not set in input parameter dict
        paramDict = {'cas_name': self.cas,
                     'cas_dir': os.path.join(dir, 'fluent/cases'),
                     #'solid_source_ip': self.inVolSource,
                     'out_temp_data': self.outTempData,
                     'out_dens_data': self.outDensData,
                     'thIters': self.thIters,
                     }
        for key, value in self.inVolSource.iteritems():
            paramDict[key] = value[0]
        injector(self.templateJournalFileName, self.journalFileName, paramDict)

    def runFluent(self):
        runDim = str(self.runDim) + 'ddp'
        subprocess.call(['fluent', runDim, '-g', '-t10', '-mpi=openmpi', '-i', str(self.journalFileName)])

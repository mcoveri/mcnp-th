#!/usr/bin/python

#################         PLOT DRIVER              ################
#
# Plots field data from MCNP and Fluent runs.  Option to plot
# 3D or 2D plots.  2D plots require location info.
#
# Changelog
# ---------
#
# Author
# ------
# William Gurecky
# william.gurecky@utexas.edu
#
#####################################################################


from gmshDriver import *
import meshData
import argparse
import os
dir = os.path.dirname(os.path.abspath(__file__))
_inputCase = None


class WriteCellData(object):
    def __init__(self, inputFile, fileType='Q', inputCase=_inputCase, **kwargs):
        self.runAsm = inputCase.geometrySetup()
        self.runAsm.buildAssembly()
        self.fieldData = meshData.MeshData(inputFile, 'fluent-2')
        self.fileType = fileType
        if fileType == 'Q':
            self.fieldData.reshapeCoords(inputCase.meshParams['TH2mcnp_reshape'])
            self.fieldData.scaleCoords(inputCase.meshParams['TH2mcnp_scale'])
            self.fieldData.shiftCoords(inputCase.meshParams['TH2mcnp_tr'])

    def findCellVals(self):
        for cell in self.runAsm.parts:
            self.fieldData.setCellProp(cell, 'temperature')

    def writeCellVals(self, outputName='temp'):
        f = open(os.path.join(dir, 'logs/cellLog_' + str(self.fileType) + '_' + str(outputName) + '.log'), 'w')
        for cell in self.runAsm.parts:
            writeLine = str(cell.cellID) + "  " + str(cell.origin[0]) + "  " + \
                str(cell.origin[1]) + "  " + str(cell.origin[2]) + "  " + \
                str(cell.temperature) + " \n"
            f.write(writeLine)
        f.close()


class Plot2D(object):
    def __init__(self, inputFile, **kwargs):
        self.data = meshData.MeshData(inputFile, 'fluent-2')
        pass


class Plot3D(object):
    def __init__(self, inputFile, **kwargs):
        self.data = meshData.MeshData(inputFile, 'fluent-2')

    def plot(self):
        self.data.plotFieldData()


def main(inputfile, dim, fileType, outFile):
    if fileType:
        outputData = WriteCellData(inputfile, fileType, _inputCase)
        outputData.findCellVals()
        outputData.writeCellVals(outFile)
    else:
        if dimension == 3:
            plotData = Plot3D(inputfile)
            plotData.plot()
        else:
            pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='2/3D plotting tool for mcnp and fluent field data.')
    parser.add_argument('-i', type=str, help='Input data log file. Required.')
    parser.add_argument('-c', type=str, help='Input python case file. Required.')
    parser.add_argument('-d', type=int, help='plot dimension')
    parser.add_argument('-w', type=str, help='write flag, either T, Q, or R')
    parser.add_argument('-l', type=str, help='2D plot locator input file')
    parser.add_argument('-o', type=str, help='Output file name')
    args = parser.parse_args()
    inputfile = args.i
    caseFile = args.c
    try:
        _inputCase = __import__(caseFile)
    except:
        sys.exit("Please supply valid python input file.")
    if not args.o:
        outFile = '1'
    else:
        outFile = args.o
    if not args.d:
        dimension = 3
    else:
        dimension = args.d
    if not args.w:
        fileType = None
    else:
        fileType = args.w
    main(inputfile, dimension, fileType, outFile)

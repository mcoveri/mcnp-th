Introduction
============

MCNP-TH is comprised of scripts to couple MCNP to ANSYS Fluent.

Only simple PWR reactor geometries are currently supported, however, extension
to complex geometries is possible by adding new GMSHpart classes.  Anything that
can be meshed in GMSH can theoredically be included in the MCNP geometry.  This
is made possible by utilizing MCNP6.1's unstructured mesh import capability by
reading ABAQUS ASCII mesh files.

The Fluent case must be manually setup.  Automated Fluent case setup is out of reach, requiring
automation of meshing, boundary condition assignment, and solver settings.  Once the Fluent case
is setup, a relatively simple Fluent journal file can be used to drive Fluent in the coupled
iteratoins.

Install
=======

Dev install python mcnpTH python package::

    python setup.py develop --user


By default, the coupling script expects the mcnp command to be named `mcnp6.mpi` and
the fluent command to be `fluent`.  These can be altered in a case input file. Otherwise
create an alias to the desired Fluent executable in the bashrc or in an env module ::

    alias fluent=/path/to/fluent/executable.exe

and create an mcnp6 alias::

    alias mcnp6.mpi=/path/to/mpi/mcnp6/executable.exe

Note: The coupling scripts have not been tested with torque/maui (or any other scheduler tech).
Coupled cases were tested on a single 64 core node.

Depends
=======

Requires ::

    ANSYS Fluent
    MCNP6.1 mpi
    python2.7
    numpy >= 1.7.0
    scipy >= 12.0

Optional ::

    dill
    periodictable

The dill package is required for restart capability.
The periodictable package is optional, if present, the atomic masses for
all isotopes in the isotope inventory will be determined by this package.
Alternatively, the user can add and edit isotopes manually in the 
materialMixer module.  The most common isotopes present in a PWR at begining
of life are included in the module by default.

Examples
========

Example cases are included in the case directory.  A complete case is comprised of
a python module file that contains geometry and solver settings, a fluent.cas file
and fluent template journal file. 

To execute a case, first change to the case directory::

    cd cases/<caseName>

and execute the case ::

    mcnpTH -i cases.<caseName>

To restart a coupled case with a restart file use the -r option::

    mcnpTH -i cases.<caseName> -r <path/to/restartFile.pickle>

Unit Tests
----------

Unit tests are located in the test folder.  Unit tests are executed as follows::

    python -m unittest test.<testModule.py>


Contact
=======

William Gurecky

william.gurecky@gmail.com


License and Credits
=====================

Fluent (R) is a registered trademark of ANSYS INC.
MCNP is developed by Los Alamos National Security, LLC for the U.S. 
Department of Energy.

MCNP-TH License
---------------

BSD

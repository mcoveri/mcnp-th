import os
import numpy as np
dir = os.path.dirname(os.path.abspath(__file__))
from gmshDriver import *


### PIN CELL TEST RUN ###
##############################
### FLUENT GEOMETRY GEN FN ###
##############################
def fluentSetup(**kwargs):
    """ Setup quarter sym pin cell case geometry. """
    # Fuel
    fuelBlocks, zircBlocks, modBlocks = [], [], []
    dz = 365.76 / 100.
    pz = 365
    zmax = 365.76 / 100.
    fuelrmax = 0.4096 / 100.
    hermax = 0.418 / 100.
    z0s = np.arange(0, zmax, dz)
    x0s = np.array([0])
    y0s = np.array([0])
    # Zirc clad
    zircrmax = 0.475 / 100.
    # Moderator
    ppitch = 1.260 / 100.
    i = 1
    j = 0
    for x0 in x0s:
        for y0 in y0s:
            for z0 in z0s:
                # fuel
                tempWedge = GMSHwedge([x0, y0, z0, 0.], [fuelrmax, np.pi / 2, dz], [6, 9, pz],
                                      source=True, cellID=i, matID=i, matName='21-fuel',
                                      density=-10.24)
                tempWedge.injectParameters()
                j = tempWedge.runPartGMSH(j, True)
                fuelBlocks.append(tempWedge)
                i += 1
                # helium
                tempRing = GMSHring([x0, y0, z0, fuelrmax, 0.], [hermax, np.pi / 2, dz], [2, 9, pz],
                                    source=False, cellID=i, matID=i, matName='02-zirc',
                                    density=-6.56)
                tempRing.injectParameters()
                j = tempRing.runPartGMSH(j, True)
                zircBlocks.append(tempRing)
                i += 1
                # cladding
                tempRing = GMSHring([x0, y0, z0, hermax, 0.], [zircrmax, np.pi / 2, dz], [3, 9, pz],
                                    source=False, cellID=i, matID=i, matName='02-zirc',
                                    density=-6.56)
                tempRing.injectParameters()
                j = tempRing.runPartGMSH(j, True)
                zircBlocks.append(tempRing)
                i += 1
                # moderator octant sym blocks
                tempOct = GMSHoctant([x0, y0, z0, zircrmax, 0.], [ppitch, 'Pi / 4.', dz], [3, 9, pz],
                                     source=False, cellID=i, matID=i, matName='00-lwtr',
                                     density=-1.0)
                tempOct.injectParameters()
                j = tempOct.runPartGMSH(j, True)
                modBlocks.append(tempOct)
                i += 1
                tempOct = GMSHoctant([x0, y0, z0, zircrmax, 'Pi / 4.'], [ppitch, 'Pi / 4.', dz], [3, 9, pz],
                                     source=False, cellID=i, matID=i, matName='00-lwtr',
                                     density=-1.0)
                tempOct.injectParameters()
                j = tempOct.runPartGMSH(j, True)
                modBlocks.append(tempOct)
                i += 1
    # Build assembly
    myAsm = GMSHassembly(fuelBlocks + zircBlocks + modBlocks)
    myAsm.buildAssembly()
    myAsm.writeAsmINP()
    return myAsm


##############################
### GEOMETRY GENERATION FN ###
##############################
def geometrySetup(**kwargs):
    """ Setup quarter sym pin cell case geometry. """
    fuelBlocks, zircBlocks, modBlocks = [], [], []
    dz = 20.32
    pz = 6
    zmax = 365.76
    fuelrmax = 0.4096
    hermax = 0.418
    z0s = np.arange(0, zmax, dz)
    x0s = np.array([0])
    y0s = np.array([0])
    # Zirc clad
    zircrmax = 0.475
    # Moderator
    ppitch = 1.260
    i = 1
    for x0 in x0s:
        for y0 in y0s:
            for z0 in z0s:
                # fuel
                tempWedge = GMSHwedge([x0, y0, z0, 0.], [fuelrmax / 2., np.pi / 2, dz], [3, 9, pz],
                                      source=True, cellID=10000 + i, matID=10000 + i, matName={'21-fuel': 1.0},
                                      density=-10.24)
                tempWedge.injectParameters()
                tempWedge.runPartGMSH()
                fuelBlocks.append(tempWedge)
                i += 1
                # fuel 2
                tempRing = GMSHring([x0, y0, z0, fuelrmax / 2., 0.], [fuelrmax, np.pi / 2, dz], [4, 9, pz],
                                    source=True, cellID=10000 + i, matID=10000 + i, matName={'21-fuel': 1.0},
                                    density=-10.24)
                tempRing.injectParameters()
                tempRing.runPartGMSH()
                fuelBlocks.append(tempRing)
                i += 1
                # cladding
                tempRing = GMSHring([x0, y0, z0, hermax, 0.], [zircrmax, np.pi / 2, dz], [2, 9, 2],
                                    source=False, cellID=i, matID=i, matName={'02-zirc': 1.0},
                                    density=-6.56)
                tempRing.injectParameters()
                tempRing.runPartGMSH()
                zircBlocks.append(tempRing)
                i += 1
                # moderator octant sym blocks
                tempOct = GMSHoctant([x0, y0, z0, zircrmax, 0.], [ppitch, 'Pi / 4.', dz], [3, 9, 2],
                                     source=False, cellID=20000 + i, matID=20000 + i, matName={'00-lwtr': 1.0},
                                     density=-0.9)
                tempOct.injectParameters()
                tempOct.runPartGMSH()
                modBlocks.append(tempOct)
                i += 1
                tempOct = GMSHoctant([x0, y0, z0, zircrmax, 'Pi / 4.'], [ppitch, 'Pi / 4.', dz], [3, 9, 2],
                                     source=False, cellID=20000 + i, matID=20000 + i, matName={'00-lwtr': 1.0},
                                     density=-0.9)
                tempOct.injectParameters()
                tempOct.runPartGMSH()
                modBlocks.append(tempOct)
                i += 1
    # Upper Plenum (16.0 cm)
    # plenH = 16.0
    # # Plen ZIRC
    # tempRing = GMSHring([x0, y0, zmax, hermax, 0.], [zircrmax, np.pi / 2, plenH], [2, 9, pz],
    #                     source=False, cellID=i, matID=i, matName={'02-zirc': 1.0},
    #                     density=-6.56)
    # tempRing.injectParameters()
    # tempRing.runPartGMSH()
    # zircBlocks.append(tempRing)
    # i += 1
    # # Plen MOD
    # tempOct = GMSHoctant([x0, y0, zmax, zircrmax, 0.], [ppitch, 'Pi / 4.', plenH], [3, 5, pz],
    #                         source=False, cellID=996, matID=996, matName={'00-lwtr': 1.0},
    #                         density=-0.8)
    # tempOct.injectParameters()
    # tempOct.runPartGMSH()
    # modBlocks.append(tempOct)
    # i += 1
    # tempOct = GMSHoctant([x0, y0, zmax, zircrmax, 'Pi / 4.'], [ppitch, 'Pi / 4.', plenH], [3, 5, pz],
    #                         source=False, cellID=997, matID=997, matName={'00-lwtr': 1.0},
    #                         density=-0.8)
    # tempOct.injectParameters()
    # tempOct.runPartGMSH()
    # modBlocks.append(tempOct)
    # i += 1
    # Lower End Gap (5.901 cm)
    # Upper nozzle (25 cm thick)
    # Upper core plate (7.6 cm thick)
    # Lower nozzle (25 cm thick)
    # Lower core plate (5 cm thick)
    # Axial Moderator reflector regions
    refThick = 30.
    # Top Moderator reflector
    topMod = GMSHcube([0, 0, zmax], [ppitch / 2., ppitch / 2., refThick], [5, 5, 5], source=False,
                      cellID=999, matID=999, matName={'00-lwtr': 1.0}, density=-0.8)
    topMod.injectParameters()
    topMod.runPartGMSH()
    modBlocks.append(topMod)
    # Bottom Mod reflector
    botMod = GMSHcube([0, 0, -refThick], [ppitch / 2., ppitch / 2., refThick], [5, 5, 5], source=False,
                      cellID=998, matID=998, matName={'00-lwtr': 1.0}, density=-0.9)
    botMod.injectParameters()
    botMod.runPartGMSH()
    modBlocks.append(botMod)
    # Build assembly
    myAsm = GMSHassembly(fuelBlocks + zircBlocks + modBlocks)
    myAsm.buildAssembly()
    myAsm.writeAsmINP()
    return myAsm

###########################
### MCNP PARAMETER DICT ###
###########################
MCNPparams = {'runName': 'pinCell',
              'eeoutFile': os.path.join(dir, 'meshes/assembly.eeout'),
              'kcode': {0: '2e4 1.0 200 500',
                        1: '2e4 1.0 300 1000',
                        3: '3e4 1.0 600 1500'
                        }
              }

#############################
### FLUENT PARAMETER DICT ###
#############################
fluentParams = {'fluentCasName': os.path.join(dir, 'fluent/cases/pinCell/pinCell.cas'),
                'templateJournal': os.path.join(dir, 'fluent/journals/template_journal_pinCell.jou'),
                'THDim': 3,
                'THheatSource': {'solid_source_ip':
                                 [os.path.join(dir, 'fluent/cases/pinCell/pinCell_source_solid.ip'),
                                  (10000, 20000),
                                  'uds-0'],
                                 'fluid_source_ip':
                                 [os.path.join(dir, 'fluent/cases/pinCell/pinCell_source_fluid.ip'),
                                  (20000, 30000),
                                  'uds-1']
                                 },
                'THdensityData': os.path.join(dir, 'fluent/cases/pinCell/pinCell_density.out'),
                'THtempData': os.path.join(dir, 'fluent/cases/pinCell/pinCell_temperature.out'),
                'THiters': 600
                }

############################
### MESH TO MESH SETINGS ###
############################
meshParams = {'mcnp2TH_tr': [0., 0., 0.],   # shift MC f6 tally result to TH ordinates [cm]
              'mcnp2TH_reshape': [0, 1, 2],          # swap z coords with y coords
              'mcnp2TH_scale': 0.01,                 # convert cm to m
              'TH2mcnp_tr': [0., 0., 0.],         # shift TH result to MCNP grid ordinates [cm]
              'TH2mcnp_reshape': [0, 1, 2],          # swaps y coords with z coords
              'TH2mcnp_scale': 100                   # convert meters to cm
              }

############################
###    COUPLER SETINGS   ###
############################
couplerParams = {'outerIters': 10,
                 'qTol': 1e-2,      # vol heat gen field convergence tolorance
                 'tTol': 1e-2,      # temperature field convergence tolorance
                 'kTol': 50e-5,
                 'iRlx': 0,         # outer iteration at which to begin back-averaging heating tally
                 'maxBI': 2,
                 'minWI': 0.5,
                 'powerNorm':  4. * 6.93e7,  # Power normalization factor (asm avg vol heat source W/m^3)
                 'logDir': os.path.join(dir, 'logs/pinCell'),
                 'updateMask': [998, 999],  # cellIDs to ignore density and temperature updates
                 'restartWrite': [2]   # outer iterations at which to write restart file
                 }

import os
import numpy as np
dir = os.path.dirname(os.path.abspath(__file__))
from gmshDriver import *


##############################
### GEOMETRY GENERATION FN ###
##############################
def geometrySetup(**kwargs):
    """ Setup 3x3 case geometry. """
    fuelBlocks, zircBlocks, modBlocks = [], [], []
    dz = 20.32
    pz = 3
    ppitch = 1.260
    zmax = 365.76
    fuelrmax = 0.4096
    hermax = 0.418
    z0s = np.arange(0, zmax, dz)
    x0s = np.array([0, ppitch, ppitch * 2.])
    y0s = np.array([0, ppitch, ppitch * 2.])
    quadrents = ['0', 'Pi / 2', 'Pi', 'Pi * (3 / 2)']
    # Zirc clad
    zircrmax = 0.475
    #zircrmax = 0.47598  # Volume corrected for discritization error
    i = 1
    for x0 in x0s:
        for y0 in y0s:
            for z0 in z0s:
                for quad in quadrents:
                    # fuel
                    tempWedge = GMSHwedge([x0, y0, z0, quad], [fuelrmax / 2., np.pi / 2, dz], [3, 6, pz],
                                          source=True, cellID=10000 + i, matID=10000 + i, matName={'21-fuel': 1.0},
                                          density=-10.24)
                    tempWedge.injectParameters()
                    tempWedge.runPartGMSH()
                    fuelBlocks.append(tempWedge)
                    i += 1
                    # fuel 2
                    tempRing = GMSHring([x0, y0, z0, fuelrmax / 2., quad], [fuelrmax, np.pi / 2, dz], [4, 6, pz],
                                        source=True, cellID=10000 + i, matID=10000 + i, matName={'21-fuel': 1.0},
                                        density=-10.24)
                    tempRing.injectParameters()
                    tempRing.runPartGMSH()
                    fuelBlocks.append(tempRing)
                    i += 1
                    # cladding
                    tempRing = GMSHring([x0, y0, z0, hermax, quad], [zircrmax, np.pi / 2, dz], [2, 6, 2],
                                        source=False, cellID=i, matID=i, matName={'02-zirc': 1.0},
                                        density=-6.56)
                    tempRing.injectParameters()
                    tempRing.runPartGMSH()
                    zircBlocks.append(tempRing)
                    i += 1
                    # moderator octant sym blocks
                    tempOct = GMSHoctant([x0, y0, z0, zircrmax, quad], [ppitch, 'Pi / 4.', dz], [3, 6, 2],
                                         source=False, cellID=i, matID=i, matName={'00-lwtr': 1.0},
                                         density=-0.88)
                    tempOct.injectParameters()
                    tempOct.runPartGMSH()
                    modBlocks.append(tempOct)
                    i += 1
                    tempOct = GMSHoctant([x0, y0, z0, zircrmax, quad + ' + Pi / 4.'], [ppitch, 'Pi / 4.', dz], [3, 6, 2],
                                         source=False, cellID=i, matID=i, matName={'00-lwtr': 1.0},
                                         density=-0.88)
                    tempOct.injectParameters()
                    tempOct.runPartGMSH()
                    modBlocks.append(tempOct)
                    i += 1
    # Axial Moderator reflector regions
    refThick = 30.
    # Top Moderator reflector
    topMod = GMSHcube([-ppitch / 2., -ppitch / 2., zmax], [3.0 * ppitch, 3.0 * ppitch, refThick],
                      [5, 5, 5], source=False, cellID=9999, matID=9999, matName={'00-lwtr': 1.0}, density=-0.7119, temperature=580.0)
    topMod.injectParameters()
    topMod.runPartGMSH()
    modBlocks.append(topMod)
    # Bottom Mod reflector
    botMod = GMSHcube([-ppitch / 2., -ppitch / 2., -refThick], [3.0 * ppitch, 3.0 * ppitch, refThick],
                      [5, 5, 5], source=False, cellID=9998, matID=9998, matName={'00-lwtr': 1.0}, density=-0.8428, temperature=500.0)
    botMod.injectParameters()
    botMod.runPartGMSH()
    modBlocks.append(botMod)
    # Build assembly
    myAsm = GMSHassembly(fuelBlocks + zircBlocks + modBlocks)
    myAsm.buildAssembly()
    myAsm.writeAsmINP()
    return myAsm

###########################
### MCNP PARAMETER DICT ###
###########################
MCNPparams = {'runName': '3x3',
              'eeoutFile': os.path.join(dir, 'meshes/3x3_assembly.eeout'),
              'kcode': {0: '1e4 1.0 150 300',
                        2: '1e4 1.0 200 500',
                        4: '2e4 1.0 150 400'
                        }
              }

#############################
### FLUENT PARAMETER DICT ###
#############################
fluentParams = {'fluentCasName': os.path.join(dir, 'fluent/cases/3x3/3x3.cas'),
                'templateJournal': os.path.join(dir, 'fluent/journals/template_journal.jou'),
                'THDim': 3,
                'THheatSource': {'solid_source_ip':
                                 [os.path.join(dir, 'fluent/cases/pinCell/pinCell_source_solid.ip'),
                                  (10000, 20000),
                                  'uds-0']
                                 },
                'THdensityData': os.path.join(dir, 'fluent/cases/3x3/3x3_density.out'),
                'THtempData': os.path.join(dir, 'fluent/cases/3x3/3x3_temperature.out'),
                'THiters': 600
                }

############################
### MESH TO MESH SETINGS ###
############################
meshParams = {'mcnp2TH_tr': [0., 0., 0.],   # shift MC f6 tally result to TH ordinates [cm]
              'mcnp2TH_reshape': [0, 1, 2],          # swap z coords with y coords
              'mcnp2TH_scale': 0.01,                 # convert cm to m
              'TH2mcnp_tr': [0., 0., 0.],         # shift TH result to MCNP grid ordinates [cm]
              'TH2mcnp_reshape': [0, 1, 2],          # swaps y coords with z coords
              'TH2mcnp_scale': 100                   # convert meters to cm
              }

############################
###    COUPLER SETINGS   ###
############################
couplerParams = {'outerIters': 10,
                 'qTol': 1e-2,      # vol heat gen field convergence tolorance
                 'tTol': 1e-2,      # temperature field convergence tolorance
                 'kTol': 50e-5,
                 'iRlx': 0,         # outer iteration at which to begin back-averaging heating tally
                 'maxBI': 2,
                 'minWI': 0.5,
                 'powerNorm':  5. * 6.73e7,  # Power normalization factor (asm avg vol heat source W/m^3)
                 'logDir': os.path.join(dir, 'logs/3x3'),
                 'updateMask': [9998, 9999],  # cellIDs to ignore density and temperature updates
                 'restartWrite': [2]   # outer iterations at which to write restart file
                 }

#!/usr/bin/python

#################           MESH DATA               ################
#
# Store and manipulates field data.  MCNP outputs vol heat souce
# (f6 tally) as fn of x,y,z.  Fluent outputs temperature field
# and fluid density filed as fn of x,y,z.  After parsing the results
# from these codes, place the result in a numpy array.  The meshData
# class makes writting field data to the mcnp Cells (set cell
# temperatures and densites) and to the .ip ANSYS Fluent ascii file
# format easy!
#
# Changelog
# ---------
#
# Author
# ------
# William Gurecky
# william.gurecky@utexas.edu
#
#####################################################################

import numpy as np
import numpy.ma as ma
from scipy.spatial import Delaunay
from scipy.interpolate import griddata
from scipy.interpolate import LinearNDInterpolator
from scipy.interpolate import NearestNDInterpolator
from gmshDriver import *
import copy
import re
import os
dir = os.path.dirname(os.path.abspath(__file__))


class MeshData(object):
    def __init__(self, dataSourceFile, dataSourceType='mcnp', **kwargs):
        self.sourceFileName = dataSourceFile
        self.sourceType = dataSourceType
        self.sourceFileLines = fileToList(self.sourceFileName)
        self.dataDim = kwargs.pop('dataDim', None)
        self.scalarField = kwargs.pop('scalarField', None)
        if dataSourceType == 'function':
            self.dataDim = 3
            self.setInterpolant(kwargs.pop('interpFn', None))
        elif dataSourceType == 'mcnp':
            self.parseEeout()
        elif dataSourceType == 'fluent':
            self.parseIp()
        else:
            self.parseFluentASCIIout()

    def writeToIP(self, ipFieldName='uds-0', **kwargs):
        """
        Write field data to Fluent14.0 ascii interpolation file.
        """
        ipFileName = kwargs.pop('ipFileName', os.path.join(dir, 'fluent/vol_heat_source.ip'))
        cellMatTRIDs = kwargs.pop('matCr', (10000, 99999))  # Selective transfer of mesh data
        ipFileLines = []
        ipFileLines.append('2 \n')  # Default Version of file for fluent14.0
        ipFileLines.append(str(self.dataDim) + ' \n')
        # FIX: No longer do we take the entire scalar field with us into
        # fluent.  We filter by material ID number.  So the length of the data
        # array must be updated accordingly
        nPoints = ma.count_masked(ma.masked_where((self.matIDField >= cellMatTRIDs[0]) & (self.matIDField <= cellMatTRIDs[1]), self.matIDField))
        ipFileLines.append(str(nPoints) + ' \n')
        #ipFileLines.append(str(len(self.scalarField[-1])) + ' \n')
        ipFileLines.append('1 \n')
        ipFileLines.append(ipFieldName + ' \n')

        def writeScalarData(data1D):
            for fieldValue, cellMatID in zip(data1D, self.matIDField):
                if cellMatID >= cellMatTRIDs[0] and cellMatID <= cellMatTRIDs[1]:
                    ipFileLines.append(str(fieldValue) + ' \n')
                else:
                    pass
        if self.dataDim == 2:
            [writeScalarData(data) for data in self.scalarField[[0, 1, -1]]]
        else:
            [writeScalarData(data) for data in self.scalarField]
        listToFile(ipFileName, ipFileLines)

    def setCellProp_depreciated(self, gmshCell, cellProp='temperature', integrationSteps=[50j, 50j, 50j]):
        """
        Set mcnp cell properties given field data from Fluent.  Compute volume
        integral by finely discretizing the mcnp cell and evaluating a nearest neighbor
        interpolant constructed inside the cell multiple times - take average to find
        the cell averaged field quantity.

        This method is broken for non-convex shapes.  Do not use.
        """
        if not hasattr(gmshCell, 'integrationLimits'):
            boundingPts = gmshCell.nodes[:, 1:]
        else:
            boundingPts = gmshCell.integrationLimits
        # construct convex hull of mcnp cell (of course, busted if shape is not convex)
        tri = Delaunay(boundingPts)
        fine_x, fine_y, fine_z = np.mgrid[min(boundingPts[:, 0]):max(boundingPts[:, 0]):integrationSteps[0],
                                          min(boundingPts[:, 1]):max(boundingPts[:, 1]):integrationSteps[1],
                                          min(boundingPts[:, 2]):max(boundingPts[:, 2]):integrationSteps[2]]
        fineCoords = (fine_x, fine_y, fine_z)
        # evaluate nearest neighbor interp at fine mesh coords
        refinedScalarField = griddata(self.scalarField[0:-1].T, self.scalarField[-1],
                                      fineCoords, method='nearest')
        fineMeshCoords = np.vstack(fineCoords).reshape(3, -1).T
        cellMask = tri.find_simplex(fineMeshCoords)
        mask = np.where(cellMask <= 0, 1, 0)
        insideVals = ma.masked_array(refinedScalarField.flatten(), mask)
        try:
            # average value inside the mcnp cell
            cellAvg = insideVals.mean()
            if cellProp == 'temperature':
                gmshCell.temperature = cellAvg
            elif cellProp == 'density':
                gmshCell.density = -cellAvg
        except:
            pass

    def setInterpolant(self, interpFn):
        self.nearNDinterpolant = interpFn

    def setCellProp(self, gmshCell, cellProp='temperature', **args):
        """
        Computes the average cell properties in a meshed (gmsh) MCNP cell.
        The elements of the mesh are passed to a Delaunay algo which computes
        the volume of each element in the MCNP cell and evaluates the scalar field
        (eg. tempearture or density field) at the mesh element centroids.  An
        average value for the scalar field withen the MCNP cell is found by a volume
        weighted average of all the constituent mesh element field values.

        This method works for _both_ tet and hexa meshes!  Convexness not required!
        """
        if not hasattr(self, 'nearNDinterpolant'):
            # nearest neighbor interpolant is required as a fallback, in case linear ND
            # interpolation fails (in cases where we try to eval the
            # interpolant oustide of the convex hull.)
            self.nearNDinterpolant = NearestNDInterpolator(self.scalarField[0:-1].T, self.scalarField[-1], rescale=True)
            # self.linNDinterpolant =
            # LinearNDInterpolator(self.scalarField[0:-1].T,
            # self.scalarField[-1], rescale=True)
        centroids, volumes = [], []
        for i, element in enumerate(gmshCell.elements):
            # element indicies as given by GMSH
            pointIndicies = element[1:] - 1
            # Node coordinates which define above element as given by GMSH
            points = gmshCell.nodes[pointIndicies][:, 1:]
            cs, vs = self._findElementVols(points)
            [centroids.append(c) for c in cs]
            [volumes.append(v) for v in vs]
        print("Processing Cell Number: " + str(gmshCell.cellID) + " ...")
        centroids = np.array(centroids)
        volumes = np.array(volumes)
        # Eval interpolant at all cell centroids
        #try:
        # elemScalarData = self.linNDinterpolant(centroids)
        #except:
        #    elemScalarData = self.nearNDinterpolant(centroids)
        elemScalarData = self.nearNDinterpolant(centroids)
        # Weight values by cell volumes to approximate intergral
        gmshCellVol = np.sum(volumes)
        cellAvg = np.sum(elemScalarData * volumes) / gmshCellVol
        if cellProp == 'temperature':
            gmshCell.temperature = cellAvg
        elif cellProp == 'density':
            gmshCell.density = -cellAvg
        print("Done with Cell!")
        return gmshCellVol, cellAvg

    def _tet_volume(self, a, b, c, d):
        """
        compute tet volume
        """
        return np.abs(np.einsum('ij, ij->i', a - d, np.cross(b - d, c - d))) / 6.

    def _findElementVols(self, points, depth=0, recursionDepthStop=1):
        """
        Computes the volume(s) and centroid(s) of a given mesh element.  could
        be a tetraherond or a hexahedron.  A hex element can be split into 2 tets.
        The centroid coordinates are used to specify a locale at which the
        linear interpolant is evaluated to obtain an estimate for the scalar field
        value inside the tet.
        """
        # QbB option scales to unit cube - important for high aspect ratio
        # cells.
        tri = Delaunay(points, qhull_options='Qbb Qc Qz QbB')
        vertices = tri.points[tri.simplices]
        sc = 1. / (tri.ndim + 1.0)
        centroids = vertices.sum(axis=1) * sc
        volumes = self._tet_volume(vertices[:, 0], vertices[:, 1],
                                   vertices[:, 2], vertices[:, 3])
        if depth == recursionDepthStop:
            return centroids, volumes
        else:
            return self._findElementVols(np.vstack((points, centroids)), depth + 1)

    def parseEeout(self, tallyType='F6'):
        """
        Parse mcnp output field data
        """
        reFlags = {'Xc': "\s+CENTROIDS\s+X",
                   'Yc': "\s+CENTROIDS\s+Y",
                   'Zc': "\s+CENTROIDS\s+Z",
                   'F6': ".+ENERGY_6",
                   'MAT': "\s+ELEMENT\sMATERIAL.*",
                   'VOL': "\s+VOLUMES.+",
                   'DEN': "\s+DENSITY.+"}
        for key, val in reFlags.iteritems():
            reFlags[key] = re.compile(val)
        for i, line in enumerate(self.sourceFileLines):
            check = checkLine(line, reFlags)
            if check:
                if check[0] == 'Xc':
                    xCentroids = self.__readEeoutBlock(self.sourceFileLines[i:])
                elif check[0] == 'Yc':
                    yCentroids = self.__readEeoutBlock(self.sourceFileLines[i:])
                elif check[0] == 'Zc':
                    zCentroids = self.__readEeoutBlock(self.sourceFileLines[i:])
                elif check[0] == 'F6':
                    f6Result = np.array(self.__readEeoutBlock(self.sourceFileLines[i:])[1:])
                elif check[0] == 'DEN':
                    matDensity = np.array(self.__readEeoutBlock(self.sourceFileLines[i:]))
                elif check[0] == 'VOL':
                    self.cellVols = np.array(self.__readEeoutBlock(self.sourceFileLines[i:]))
                elif check[0] == 'MAT':
                    matIDs = np.array(self.__readEeoutBlock(self.sourceFileLines[i:]))
            else:
                pass
        if len(xCentroids) == len(f6Result) and len(yCentroids) == len(f6Result) \
           and len(matDensity) == len(f6Result) and len(matIDs) == len(xCentroids):
            f6Result *= matDensity
            self.matIDField = matIDs
            self.scalarField = np.array([xCentroids,
                                        yCentroids,
                                        zCentroids,
                                        f6Result])
        else:
            self.scalarField = None
            print("Could not read scalar data from eeout file. Data length mismatch")
        if len(np.unique(yCentroids)) == 1 or len(np.unique(xCentroids)) == 1 \
           or len(np.unique(zCentroids)) == 1:
            self.dataDim = 2
        else:
            self.dataDim = 3

    def __readEeoutBlock(self, lines):
        vals = None
        dataBlock = []
        for line in lines:
            if line[0:8] != "        ":
                try:
                    vals = [float(word) for word in line.split()]
                    dataBlock += vals
                except:
                    pass
            else:
                # is vals defined?  If yes, we were already in the thick of it
                if vals:
                    break
                else:
                    pass
        return np.array(dataBlock)

    def normScalarField(self, c=1.0, preNorm=False):
        """ Scales scalar field values by c. """
        if preNorm == 'sum':
            try:
                self.scalarField[-1] *= self.cellVols  # scale by cell vols
                self.scalarField[-1] *= (1.0 / np.sum(self.scalarField[-1]))
                self.scalarField[-1] *= c              # Total energy must sum to c
                self.scalarField[-1] /= (self.cellVols / 1.e6)  # Need in units of Pwr/Vol [w/m3]
            except:
                sys.exit("Cell volumes are not avalible.")
        elif preNorm == 'avg':
            self.scalarField[-1] *= (1.0 / np.mean(self.scalarField[-1]))
            self.scalarField[-1] *= c   # avg field value must be equal to c
        else:
            self.scalarField[-1] *= c
        return self.scalarField

    def scaleCoords(self, c):
        """ Scales all coordinate vectors by c.  Useful for converting m -> cm
        or vica versa.
        """
        self.scalarField[0:-1] *= c
        return self.scalarField

    def shiftCoords(self, shiftVector=np.array([0., 0., 0.])):
        """ Translates scalar field """
        for i, s in enumerate(shiftVector):
            self.scalarField[i] += s

    def reshapeCoords(self, reshape=[0, 2, 1]):
        """ Swap coordinates with each other: eg  y <-> z. """
        tmpScalarField = [[], [], [], []]
        for i in reshape:
            tmpScalarField[i] = copy.deepcopy(self.scalarField[reshape[i]])
        for i in reshape:
            self.scalarField[i] = tmpScalarField[i]

    def dataNorm(self):
        return np.linalg.norm(self.scalarField[-1])

    def parseIp(self, startLine=5):
        """
        Parse fluent interpolation file.
        """
        self.dataDim = int(self.sourceFileLines[1])
        nPoints = int(self.sourceFileLines[2])
        tmpLines = self.sourceFileLines[startLine:]
        xB = [0, nPoints]
        yB = [nPoints, nPoints * 2]
        zB = [nPoints * 2, nPoints * 3]
        e3B = [nPoints * 3]
        e2B = [nPoints * 2]
        xCentroids = np.array([float(line.strip()) for line in tmpLines[xB[0]:xB[1]]])
        yCentroids = np.array([float(line.strip()) for line in tmpLines[yB[0]:yB[1]]])
        if self.dataDim == 3:
            zCentroids = np.array([float(line.strip()) for line in tmpLines[zB[0]:zB[1]]])
            result = np.array([float(line.strip()) for line in tmpLines[e3B[0]:]])
        else:
            result = np.array([float(line.strip()) for line in tmpLines[e2B[0]:]])
            zCentroids = np.zeros(len(result))
        if len(result) == len(xCentroids) and len(result) == len(yCentroids):
            self.scalarField = np.array([xCentroids,
                                        yCentroids,
                                        zCentroids,
                                        result])
        else:
            print("IP Parse error. Ensure IP file only has data for a single scalar field.")
            self.scalarField = None

    def parseFluentASCIIout(self, startLine=1):
        """ Parse ascii fluent output field file. """
        importedFieldData = np.loadtxt(self.sourceFileLines[startLine:])
        if importedFieldData.shape[1] == 4:
            self.dataDim = 2
            self.scalarField = np.array([np.around(importedFieldData[:, 1], 9),
                                         np.around(importedFieldData[:, 2], 9),
                                         np.zeros(len(importedFieldData[:, 1])),
                                         importedFieldData[:, -1]])
        else:
            self.dataDim = 3
            self.scalarField = np.array([np.around(importedFieldData[:, 1], 9),
                                         np.around(importedFieldData[:, 2], 9),
                                         np.around(importedFieldData[:, 3], 9),
                                         importedFieldData[:, -1]])

    def __genMayaviScalarField(self):
        """
        Makes mayavi2 scalar field data set for visualization.
        Generates data on a fine regular mesh.  This is very useful for mesh to mesh
        interpolation.
        """
        # Generate fine regular grid
        fine_x, fine_y, fine_z = np.mgrid[min(self.scalarField[0]):max(self.scalarField[0]):50j,
                                          min(self.scalarField[1]):max(self.scalarField[1]):50j,
                                          min(self.scalarField[2]):max(self.scalarField[2]):500j]
        fineCoords = (fine_x, fine_y, fine_z)
        self.mayaviScalarField = griddata(self.scalarField[0:-1].T, self.scalarField[-1],
                                          fineCoords, method='nearest', rescale=True)

    def writeScalarFieldToFile(self, outputFile):
        output = np.vstack((np.arange(len(self.scalarField[0])), self.scalarField)).T
        np.savetxt(outputFile, output, header='c Output from MeshData class')

    def plotFieldData(self):
        """
        Use mayavi2 to visualize scalar field.
        """
        from mayavi import mlab
        if not hasattr(self, 'mayaviScalarField'):
            self.__genMayaviScalarField()
        scalar = mlab.pipeline.scalar_field(self.mayaviScalarField)
        mlab.outline()
        mlab.show()

if __name__ == "__main__":
    eeoutFile = os.path.join(dir, 'meshes/assembly.eeout')
    asciiOut = os.path.join(dir, 'fluent/examples/density.out')
    ipFile = os.path.join(dir, 'fluent/examples/solid_temp_out.ip')
    #myScalarField = MeshData(eeoutFile, 'mcnp')
    #myScalarField.plotFieldData()
    #myTField = MeshData(ipFile, 'fluent')
    #myTField.scaleCoords(100.)
    #myTField.reshapeCoords()
    #myTField.shiftCoords([1.0, 0.25, 0.])  # move the 2D fluent data right 1, and 'back' 0.25 cm
    #myTestCube = GMSHcube([0, 0, 0], [1, 1, 10], [5, 1, 5], source=True,
    #                      cellID=1, matID=1, matName='21-fuel', density=-10.24)
    #myTestCube.injectParameters()
    #myTestCube.runPartGMSH()
    #print("Previous cell temperature")
    #print(myTestCube.temperature)
    #myTField.setCellProp(myTestCube)
    #print("New cell temperature")
    #print(myTestCube.temperature)
    #myTField.plotFieldData()
    #myTField.writeToIP()
    myDensityField = MeshData(asciiOut, 'fluent')

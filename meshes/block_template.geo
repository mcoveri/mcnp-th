cl__1 = 0.01;
Geometry.CopyMeshingMethod = 1;

// location of box origin
x0 = <x0> ;
y0 = <y0> ;
z0 = <z0> ;
// Variable dimensions
dx = <dx> ;
dy = <dy> ;
dz = <dz> ;
// Number of Partitions
px = <px> ;
py = <py> ;
pz = <pz> ;

zBlockHeight = z0 + dz;
depth = y0 + dy;
width = x0 + dx;

// Fuel Box Geometry
Point(1) = {x0, y0, z0, 1};
Point(2) = {width, y0, z0, 1};
Point(3) = {width, depth, z0, 1};
Point(4) = {x0, depth, z0, 1};

// Translated z+ points
Point(5) = {x0, depth, zBlockHeight, 1};
Point(6) = {x0, y0, zBlockHeight, 1};
Point(10) = {width, y0, zBlockHeight, 1};
Point(14) = {width, depth, zBlockHeight, 1};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line(8) = {5, 6};
Line(9) = {6, 10};
Line(10) = {10, 14};
Line(11) = {14, 5};
Line(13) = {4, 5};
Line(14) = {1, 6};
Line(18) = {2, 10};
Line(22) = {3, 14};
Line Loop(6) = {4, 1, 2, 3};
Plane Surface(6) = {6};
Line Loop(15) = {4, 14, -8, -13};
Ruled Surface(15) = {15};
Line Loop(19) = {1, 18, -9, -14};
Ruled Surface(19) = {19};
Line Loop(23) = {2, 22, -10, -18};
Ruled Surface(23) = {23};
Line Loop(27) = {3, 13, -11, -22};
Ruled Surface(27) = {27};
Line Loop(28) = {8, 9, 10, 11};
Plane Surface(28) = {28};
Surface Loop(1) = {6, 28, 15, 19, 23, 27};
Volume(1) = {1};

// Define structured mesh
Transfinite Line {9} = px Using Progression 1;
Transfinite Line {10} = py Using Progression 1;
Transfinite Line {11} = px Using Progression 1;
Transfinite Line {8} = py Using Progression 1;
Transfinite Line {18} = pz Using Progression 1;
Transfinite Line {22} = pz Using Progression 1;
Transfinite Line {13} = pz Using Progression 1;
Transfinite Line {14} = pz Using Progression 1;
Transfinite Line {1} = px Using Progression 1;
Transfinite Line {2} = py Using Progression 1;
Transfinite Line {3} = px Using Progression 1;
Transfinite Line {4} = py Using Progression 1;
Transfinite Surface {28};
Transfinite Surface {23};
Transfinite Surface {6};
Transfinite Surface {15};
Transfinite Surface {19};
Transfinite Surface {27};
Recombine Surface {15, 28, 23, 6, 19, 27};
Transfinite Volume{1} = {6, 10, 14, 5, 1, 2, 3, 4};

// Label physical volumes according to AAAAAAA_ZZZ format
// Where AAAAAA is 'material'  and ZZZ if material ID used in mcnp
Physical Volume("material_<matID>") = {1};

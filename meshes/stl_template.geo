Merge "<stlFile>";
Mesh.CharacteristicLengthExtendFromBoundary = 0;

// Set Meshing opts
Mesh.SubdivisionAlgorithm = 2;
Geometry.Tolerance = 1e-08;

// Create vol
Surface Loop(2) = {1};
Volume(3) = {2};
Physical Volume("material_<matID>") = {3};

// Apply Translations
Translate {<dx>, <dy>, <dz>} {
  Volume{3};
}

// Apply Dialation
Dilate {{<ix>, <iy>, <iz>}, <sc>} {
  Volume{3};
}

// Apply Rotations
// rotation axis, any point on axis, rotation in radians
Rotate {{<ax>, <ay>, <az>}, {<p1>, <p2>, <p3>}, <rt>} {
  Volume{3};
}

Mesh.SubdivisionAlgorithm = 2;
Geometry.Tolerance = 1e-8;
// origin
x0 = 1.5;
y0 = 1.5;
z0 = 1.5;
t0 = 0.0;   // theta 0 in xy plane

// variable dimenstions
dr = 0.1;   // wedge radius
dt = Pi * 2;  // wedge azmuthal len in radians
dz = 0.1;   // axial height

// mesh partitions
pr = 5;  // radial partions
pa = 9;  // azumuthal partitions
pz = 5;  // z partions

zmax = z0 + dz;   // zmax
tmax = t0 + dt;   // theta max
x1 = x0 + dr * Cos(t0);
x2 = x0 + dr * Cos(tmax);
y1 = y0 + dr * Sin(t0);
y2 = y0 + dr * Sin(tmax);

// Points
Point(1) = {x0, y0, z0, 0.1};
Point(2) = {x1, y1, z0, 0.1}; 
Point(3) = {x2, y2, z0, 0.1}; 
//
Point(4) = {x0, y0, zmax, 0.1};
Point(5) = {x1, y1, zmax, 0.1};
Point(6) = {x2, y2, zmax, 0.1};
//
Line(1) = {1,2};
Line(2) = {1,3};
Circle(3) = {2, 1, 3};
//
Line(4) = {1,4};
Line(5) = {4,5};
Line(6) = {2,5};
Line(7) = {3,6};
Circle(8) = {5, 4, 6};
Line(9) = {4,6};

Line Loop(10) = {5,-6,-1,4};
Ruled Surface(11) = {10};
Line Loop(12) = {-9,7,2,-4};
Ruled Surface(13) = {12};
Line Loop(14) = {-6,-8,7,3};
Ruled Surface(15) = {14};

Line Loop(16) = {8,-9,5};
Plane Surface(17) = {16};

Line Loop(18) = {1,3,-2};
Plane Surface(19) = {18};

Surface Loop(20) = {17,15,-11,-19,-13};
Volume(21) = {20};

// Transfinite Line {9,5,8,2,1,3,4,6,7} = 4 Using Progression 1;
Transfinite Line {9} = pr Using Progression 1;  // radial
Transfinite Line {5} = pr Using Progression 1;  // radial
Transfinite Line {8} = pa Using Progression 1;  // circle
Transfinite Line {2} = pr Using Progression 1;  // radial
Transfinite Line {1} = pr Using Progression 1;  // radial
Transfinite Line {3} = pa Using Progression 1;  // circle
Transfinite Line {4} = pz Using Progression 1;  // z+
Transfinite Line {6} = pz Using Progression 1;  // z+
Transfinite Line {7} = pz Using Progression 1;  // z+
Transfinite Surface {11} = {1,2,5,4};
Transfinite Surface {13} = {1,3,6,4};
Transfinite Surface {15} = {3,2,5,6};
Transfinite Surface {19} = {1,2,3};
Transfinite Surface {17} = {4,5,6};
Recombine Surface {11,13,15,19,17};

Transfinite Volume{21} = {1,2,3,4,5,6};

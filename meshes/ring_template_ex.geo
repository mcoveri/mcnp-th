Mesh.SubdivisionAlgorithm = 2;
Geometry.Tolerance = 1e-8;
// origin
x0 = 0.;
y0 = 0.;
z0 = 0.;
t0 = 0.;   // theta 0 in xy plane

// variable dimenstions
r0 = 0.05;  // ring inner radius
dr = 0.1;   // ring outer radius
dt = Pi / 2;  // wedge azmuthal len in radians
dz = 0.1;   // axial height

// mesh partitions
pr = 5;  // radial partions
pa = 9;  // azumuthal partitions
pz = 5;  // z partions

zmax = z0 + dz;   // zmax
tmax = t0 + dt;   // theta max

x1_1 = x0 + r0 * Cos(t0);
x1_2 = x0 + r0 * Cos(tmax);
y1_1 = y0 + r0 * Sin(t0);
y1_2 = y0 + r0 * Sin(tmax);

x1 = x0 + dr * Cos(t0);
x2 = x0 + dr * Cos(tmax);
y1 = y0 + dr * Sin(t0);
y2 = y0 + dr * Sin(tmax);

// low plane
Point(10) = {x0, y0, z0, 0.1};  // low origin
Point(11) = {x1_1, y1_1, z0, 0.1};  // near arc
Point(12) = {x1_2, y1_2, z0, 0.1};
Point(2) = {x1, y1, z0, 0.1};   // far arc
Point(3) = {x2, y2, z0, 0.1}; 
// high plane
Point(40) = {x0, y0, zmax, 0.1};  // high origin
Point(41) = {x1_1, y1_1, zmax, 0.1};  // near arc
Point(42) = {x1_2, y1_2, zmax, 0.1};
Point(5) = {x1, y1, zmax, 0.1};  // far arc
Point(6) = {x2, y2, zmax, 0.1};
//
// Bottom plane
Line(1) = {11, 2};  // inside to out
Circle(2) = {2, 10, 3};   // outer arc
Line(3) = {12, 3};  // inside to out
Circle(4) = {11, 10, 12};   // inner arc

// Top plane
Line(5) = {41, 5};
Circle(6) = {5, 40, 6};
Line(7) = {42, 6};
Circle(8) = {41, 40, 42};

// Connecting dz lines
Line(9) = {11, 41};  // inner lines
Line(10) = {12, 42};
Line(11) = {2, 5};  // far lines
Line(12) = {3, 6};

// Bottom surface
Line Loop(10) = {1, 2, -3, -4};
Ruled Surface(10) = {10};
// Top Surface
Line Loop(11) = {5, 6, -7, -8};
Ruled Surface(11) = {11};
// X dz plane
Line Loop(12) = {9, 5, -11, -1};
Plane Surface(12) = {12};
// Y dz plane
Line Loop(13) =  {10, 7, -12, -3};
Plane Surface(13) = {13};
//
// Inside concave face
Line Loop(14) = {9, 8, -10, -4};
Ruled Surface(14) = {14};

// Outside convex face
Line Loop(15) = {11, 6, -12, -2};
Ruled Surface(15) = {15};

Surface Loop(20) = {14, 12, -15, -13, -11, 10};
Volume(21) = {20};

// Transfinite defs
Transfinite Line {9} = pz Using Progression 1;
Transfinite Line {10} = pz Using Progression 1;
Transfinite Line {11} = pz Using Progression 1;
Transfinite Line {12} = pz Using Progression 1;
Transfinite Line {1, 3, 5, 7} = pr Using Progression 1;
Transfinite Line {2, 4, 6, 8} = pa Using Progression 1;
Transfinite Surface {10} = {11, 12, 2, 3};
Transfinite Surface {11} = {41, 42, 5, 6};
Transfinite Surface {12} = {11, 41, 5, 2};
Transfinite Surface {13} = {12, 42, 6, 3};
Transfinite Surface {14} = {41, 42, 12, 11};
Transfinite Surface {15} = {2, 5, 6, 3};
Recombine Surface {10, 11, 12, 13, 14, 15};
// Transfinite Volume {21} = {11, 12, 2, 3, 41, 42, 5, 6};
Transfinite Volume {21};

import os
dir = os.path.dirname(os.path.abspath(__file__))
from gmshDriver import *

### DEFAULT PLANE WALL RUN ###


##############################
### GEOMETRY GENERATION FN ###
##############################
def geometrySetup(**kwargs):
    """ Setup plane wall case geometry. """
    # create basic block geometry
    # Fuel
    fuelBlocks = []
    dz = 30
    dx = 0.25
    fuelzmax = 300
    fuelxmax = 0.5
    # Block origins
    z0s = np.arange(0, fuelzmax, dz)
    x0s = np.arange(0, fuelxmax, dx)
    y0s = np.array([0])
    i = 1
    for x0 in x0s:
        for y0 in y0s:
            for z0 in z0s:
                tempCube = GMSHcube([x0, y0, z0], [dx, 1, dz], [5, 1, 5], source=True,
                                    cellID=i, matID=i, matName='21-fuel', density=-10.24)
                tempCube.injectParameters()
                tempCube.runPartGMSH()
                fuelBlocks.append(tempCube)
                i += 1
    # Moderator
    modBlocks = []
    dz = 30
    dx = 1.0
    modzmax = 300
    modxmax = fuelxmax + 1.0
    z0s = np.arange(0, modzmax, dz)
    x0s = np.arange(fuelxmax, modxmax, dx)
    y0s = np.array([0])
    for x0 in x0s:
        for y0 in y0s:
            for z0 in z0s:
                tempCube = GMSHcube([x0, y0, z0], [dx, 1, dz], [5, 1, 5], source=False,
                                    cellID=i, matID=i, matName='00-lwtr', density=-1.0)
                tempCube.injectParameters()
                tempCube.runPartGMSH()
                modBlocks.append(tempCube)
                i += 1
    # Top Moderator
    topMod = GMSHcube([0, 0, 300], [1.5, 1, 30], [5, 1, 5], source=False,
                      cellID=50, matID=50, matName='00-lwtr', density=-1.0)
    topMod.injectParameters()
    topMod.runPartGMSH()
    modBlocks.append(topMod)
    # Bottom Mod
    botMod = GMSHcube([0, 0, -30], [1.5, 1, 30], [5, 1, 5], source=False,
                      cellID=51, matID=51, matName='00-lwtr', density=-1.0)
    botMod.injectParameters()
    botMod.runPartGMSH()
    modBlocks.append(botMod)
    # Build assembly
    myAsm = GMSHassembly(fuelBlocks + modBlocks)
    #myAsm.buildAssembly()
    #myAsm.writeAsmINP()
    return myAsm

###########################
### MCNP PARAMETER DICT ###
###########################
MCNPparams = {'runName': 'planeWallRun',
              'eeoutFile': os.path.join(dir, 'meshes/assembly.eeout'),
              'kcode': {0: '5e4 1.0 50 100',
                        2: '5e4 1.0 70 120',
                        4: '5e4 1.0 80 300'
                        }
              }

#############################
### FLUENT PARAMETER DICT ###
#############################
fluentParams = {'fluentCasName': os.path.join(dir, 'fluent/cases/plane_wall_combo.cas'),
                'templateJournal': os.path.join(dir, 'fluent/journals/template_journal.jou'),
                'THDim': 2,
                'THheatSource': os.path.join(dir, 'fluent/cases/planeWall/plane_wall_source.ip'),
                'THdensityData': os.path.join(dir, 'fluent/cases/planeWall/plane_wall_density.out'),
                'THtempData': os.path.join(dir, 'fluent/cases/planeWall/plane_wall_temperature.out'),
                'THiters': 800
                }

############################
### MESH TO MESH SETINGS ###
############################
meshParams = {'mcnp2TH_tr': [-0.5 / 100., 0., 0.],   # shift MC f6 tally result to TH ordinates [cm]
              'mcnp2TH_reshape': [0, 2, 1],          # swap z coords with y coords
              'mcnp2TH_scale': 0.01,                 # convert cm to m
              'TH2mcnp_tr': [0.5, 0.25, 0.],         # shift TH result to MCNP grid ordinates [cm]
              'TH2mcnp_reshape': [0, 2, 1],          # swaps y coords with z coords
              'TH2mcnp_scale': 100                   # convert meters to cm
              }

############################
###    COUPLER SETINGS   ###
############################
couplerParams = {'outerIters': 6,
                 'qTol': 1e-3,      # vol heat gen field convergence tolorance
                 'tTol': 1e-3,      # temperature field convergence tolorance
                 'kTol': 50e-5,
                 'iRlx': 2,         # outer iteration at which to begin back-averaging heating tally
                 'powerNorm':  6.0e7,  # Power normalization factor (asm avg vol heat source W/m^3)
                 'logDir': os.path.join(dir, 'logs'),
                 'updateMask': [50, 51]  # cellIDs to ignore density and temperature updates
                 }

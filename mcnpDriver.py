#!/usr/bin/python

#################           MCNP6 DRIVER              ################
#
# Composes complete MCNP input decks.
# Adds ability to parse output eeout file to vol heat source field
# data structure.
#
# Changelog
# ---------
#
# TODO: Write Keff parser
#
# Author
# ------
# William Gurecky
# william.gurecky@utexas.edu
#
#####################################################################

import copy
import re
import sys
import numpy as np
import subprocess
import os
from gmshDriver import *
from materialMixer import *
dir = os.path.dirname(os.path.abspath(__file__))


class MCNPdriver(object):
    """
    Builds MCNP deck given material and cell information from
    a GMSH assembly. Calls mcnp6.1 to run the deck.
    Parses and stores the F6 heating tally result.
    """
    def __init__(self, gmshAsm, **kwargs):
        self.gmshAsm = gmshAsm
        self.runName = kwargs.pop('runName', 'blockRun')
        self.kcode = kwargs.pop('kcode', '5e4 1.0 25 60')
        self.cores = kwargs.pop('cores', 60)
        self.gamma = kwargs.pop('gamma', False)  # gamma transport flag, default off
        self.srctpFileName = kwargs.pop('srctp', None)
        self.mcnpIters = 0
        # self.mcnpOutputFileName = os.path.join(dir, "mcnp/" + self.runName + ".o")
        # Try the mcnp output n opt
        mcnpBaseDir = kwargs.pop('mcnpBaseDir', dir)
        self.mcnpOutputFileName = os.path.join(mcnpBaseDir, "mcnp/" + self.runName + ".")
        self.dataCardFile = os.path.join(mcnpBaseDir, "mcnp/dataCards/" + self.runName + ".dat")
        self.mcnpInputFileName = os.path.join(mcnpBaseDir, "mcnp/" + self.runName + ".i")

    def updateKcode(self, kcode):
        self.kcode = kcode

    def setDataCards(self, dataCards=None):
        """
        Set default data cards based on material composition
        of the cells in the assembly.  Optionally supply additional
        data cards.

        Every gmsh part in the asm has its own material.  To determine
        the material we need a material name string e.g:
            24-fuel (for 2.4 enriched UO2 fuel)
            00-lwtr (for standard lwtr w no boron)
        Pre defined materials are stored in a material dictionary.
        Adding new material mixtures requires
        Ensure the material identifiers given in to each of the gmsh parts
        exists in the material dictionary.
        Also need the cell's temperature so we may determine the proper
        mass wighted cross sections to use in the final MCNP material card.
        """
        self.matCards, impCards, tmpCards = [], [], []
        if self.gamma:
            transportMode = ['n', 'p']
        else:
            transportMode = ['n']
        if dataCards is None:
            # Default additional dataCards
            self.matCards.append('mode ' + ' '.join(transportMode) + ' \n')
            self.matCards.append('kcode ' + self.kcode + ' \n')
            self.matCards.append('embee6:' + ''.join(transportMode) + ' embed=1 \n')
            # self.matCards.append('PRDMP 0 0 0 0 150 \n')  # cycle dump
        else:
            # Custom data card definition possible
            for line in dataCards:
                self.matCards.append(line)
        i = 0
        for cell in self.gmshAsm.parts:
            self.matCards += self.materialMix(cell)[0]
            if i == 0:
                tmpCards.append('TMP  ' + str(self.__tConvert(cell.temperature)) + ' \n')
                impCards.append('inp:n ' + str(1) + ' \n')
            else:
                tmpCards.append('     ' + str(self.__tConvert(cell.temperature)) + ' \n')
                impCards.append('      ' + str(1) + ' \n')
            i += 1
        impCards.append('      ' + str(1) + ' \n')  # background cell imp
        impCards.append('      ' + str(1) + ' \n')  # embeded mesh imp
        impCards.append('      ' + str(0) + ' \n')  # graveyard
        listToFile(self.dataCardFile, self.matCards + tmpCards)

    def materialMix(self, cell):
        """
        Generates mass weighted material mixture according to cell temperature.
        Homogenize material mixture by weight frac.
        eg.
            A mixture of 0.8 stainless and 0.2 light water by mass would be given as
            {'00-lwtr': 0.2, 'ss': 0.8}
            when assigning a material to a given cell.
            when mixed, the homogenized material name is:
                00-lwtr-ss
            This mixture's isotopics are computed and cross sections are selected
            and weighted according to the cell's temperature.
        """
        pseudoMatDict = {}
        for matName, matProp in cell.materialName.iteritems():
            if type(matProp) is tuple:
                pseudoMatDict[matName] = self.__pseudoMatMix(cell, matName) * matProp[0]
                pseudoMatDict[matName].setDensity(matProp[1] * matProp[0])
            else:
                pseudoMatDict[matName] = self.__pseudoMatMix(cell, matName) * matProp
        # Create new pseudo mixed material
        pseudoMixedMat = reduce(lambda x, y: x + y, pseudoMatDict.values())
        pseudoMixedMat.setDensity(abs(cell.density))
        # pseudo mixed mat name is generated by concatenating
        # constituent material strings
        pseudoMixedMatName = '-'.join(pseudoMatDict.keys())
        if np.isnan(sum(pseudoMixedMat.nDdict.values())):
            import pdb; pdb.set_trace()  # XXX BREAKPOINT
            sys.exit("Mat mixxer encountered a divide by zero error")
        return self.__writePseudoMatCards(cell, pseudoMixedMat, pseudoMixedMatName)

    def __pseudoMatMix(self, cell, matName):
        """
        Given cell temperature, determine wighted material mixture by the
        pseudo material method.
        """
        # Find bounding temperatures
        # TODO: move all mixtureData[matName] into GMSHpart
        # class.  Each cell should have a mixed material assigned to it!
        # For now this hacky way of assigning materials to cells is OK
        tempList = np.sort(np.array(mixtureData[matName]['xsT'].keys()))
        upperIndex = np.searchsorted(tempList, cell.temperature)
        lowerIndex = upperIndex - 1
        if upperIndex == len(tempList):
            w_high, w_low = 1.0, 0.0
            cellThigh, cellTlow = tempList[-1], tempList[-2]
        elif lowerIndex == 0 and cell.temperature <= np.sort(tempList)[0]:
            w_high, w_low = 0.0, 1.0
            cellThigh, cellTlow = tempList[1], tempList[0]
        else:
            cellThigh, cellTlow = tempList[upperIndex], tempList[lowerIndex]
            w_high = 1 - (np.sqrt(cellThigh) - np.sqrt(cell.temperature)) / (np.sqrt(cellThigh) - np.sqrt(cellTlow))
            w_low = abs(1.0 - w_high)
        # Weight cross sections
        lowXsMat = copy.deepcopy(mixtureData[matName][cellTlow])
        lowXsMat.setDensity(abs(cell.density))
        highXsMat = copy.deepcopy(mixtureData[matName][cellThigh])
        highXsMat.setDensity(abs(cell.density))
        if w_high == 0:
            pseudoMat = lowXsMat * w_low
        elif w_low == 0:
            pseudoMat = highXsMat * w_high
        else:
            pseudoMat = highXsMat * w_high + lowXsMat * w_low
        if np.isnan(sum(pseudoMat.nDdict.values())):
            import pdb; pdb.set_trace()  # XXX BREAKPOINT
            sys.exit("Mat mixxer encountered a divide by zero error")
        pseudoMat.setDensity(abs(cell.density))
        return pseudoMat

    def __writePseudoMatCards(self, cell, pseudoMixedMat, matName):
        """
        Generate material cards for MCNP deck for the pseudo material mixture.
        """
        i, matCards = 0, []
        matCards.append('c \n')
        for nuclide, numberDensity in pseudoMixedMat.nDdict.iteritems():
            # MCNP barfs if material has 0.0 atom frac in M card
            if numberDensity != 0.0:
                if i == 0:
                    matCards.append('M' + str(cell.matID) + '    ' + str(nuclide) +
                                    '    ' + str(numberDensity) + ' \n')
                else:
                    matCards.append('       ' + str(nuclide) + '    ' + str(numberDensity) + ' \n')
                i += 1
        # Print nearest s(a,b) card for light water
        if 'lwtr' in matName:
            lwtrDict = {293.6: '10t', 350.0: '11t', 400.0: '12t', 450.0: '13t',
                        500.0: '14t', 550.0: '15t', 600.0: '16t'}
            tf = lambda array, value: array[(np.abs(array - value)).argmin()]
            nearestTemp = tf(np.array(lwtrDict.keys()), cell.temperature)
            matCards.append('mt' + str(cell.matID) + ' lwtr.' + lwtrDict[nearestTemp] + ' \n')
        # Return completed composition
        return matCards, pseudoMixedMat.nDdict

    def __tConvert(self, T):
        """ For TMP (free gas thermal temperature) card """
        return T * 8.617e-11

    def setBCs(self, boundGeom=None):
        """ Replaces generic background sphere with background
        box with reflective radial surfaces. bounding cell geometry
        may be user input if reactor geometry is complex. """
        # get max extent of problem in xyz dirs
        self.meshBounds = self.modelBounds()
        # replace SPH card with custom geom cards
        for i, line in enumerate(self.mcnpInput):
            if line.startswith("c SURFACES"):
                surfaceLine = i + 1
                break
        del self.mcnpInput[surfaceLine]
        #
        if boundGeom is None or boundGeom == 'void':
            boundGeom = {999: [-1, '*', 'RPP', str(self.meshBounds[0]), str(self.meshBounds[1]),
                               str(self.meshBounds[2]), str(self.meshBounds[3]),
                               str(0), str(0)],
                         1000: [1, '', 'PZ', str(self.meshBounds[4])],
                         1001: [-1, '', 'PZ', str(self.meshBounds[5])]}
        else:
            boundGeom = {999: [-1, '*', 'RPP', str(self.meshBounds[0]), str(self.meshBounds[1]),
                               str(self.meshBounds[2]), str(self.meshBounds[3]),
                               str(0), str(0)],
                         1000: [1, '*', 'PZ', str(self.meshBounds[4])],
                         1001: [-1, '*', 'PZ', str(self.meshBounds[5])]}
        self.__updateLegacyCells(boundGeom)
        self.__sourceModifier()
        for cellID, geom in boundGeom.iteritems():
            geomLine = str(geom[1]) + str(cellID) + '  ' + '  '.join(geom[2:]) + ' \n'
            self.mcnpInput.insert(surfaceLine, geomLine)
        listToFile(self.mcnpInputFileName, self.mcnpInput)

    def __updateLegacyCells(self, boolGeom):
        """ Updates legacy cell cards.
        Takes boolean geometry. bool Geom is dict:
            {<geomID> : -1..., <geomID>: 1 ... }
            where vals are "inside" geom delimiter """
        for i, line in enumerate(self.mcnpInput):
            if line.startswith("c LEGACY"):
                legacyLine = i
                fillCellLine = self.mcnpInput[i + 1].split()
                boundCellLine = self.mcnpInput[i + 2].split()
                break
        csgFill, csgBound = [], []
        for geomID, bI in boolGeom.iteritems():
            csgFill.append(str(int(geomID) * int(bI[0])))
            csgBound.append(str(int(geomID) * int(bI[0]) * -1))
        fillCellLine[2] = ' '.join(csgFill)
        boundCellLine[2] = ':'.join(csgBound)
        self.mcnpInput[legacyLine + 1] = '  '.join(fillCellLine) + ' \n'
        self.mcnpInput[legacyLine + 2] = '  '.join(boundCellLine) + ' \n'

        # REMOVE default f4 tally generated by UM_PRE_OP program
        for i, line in enumerate(self.mcnpInput):
            if line.startswith("embee4:n"):
                self.mcnpInput[i] = "c embee4:n embed=1 \n"
                break
            else:
                pass

    def __sourceModifier(self):
        """ If a scrtp file from previous iter is found, no need to include an
        sdef card in the input deck. """
        # TODO this techincally works, but after the 1st iteration, mcnp
        # writes srctp file to *.s.bak file, so the *.s srctp file will not
        # exist after iter 1.  We set srctpFileName to *.s but this is
        # confusing because in reality, after iter 1, it it *.s.bak
        previousSrctpFile = os.path.exists(self.mcnpOutputFileName + 's')
        if self.mcnpIters >= 1 and previousSrctpFile:
            self.srctpFileName = self.mcnpOutputFileName + 's'
        else:
            pass
        sdefLine = None
        for i, line in enumerate(self.mcnpInput):
            if line.startswith("sdef"):
                sdefLine = i
                break
        if self.srctpFileName and sdefLine:
            self.mcnpInput[sdefLine] = 'c sdef pos= volumer \n'

    def runUmPreOp(self, customDataCards=None):
        """
        Executes MCNP's unstructured mesh pre processor to generate a
        partially complete mcnp input deck
        """
        self.setDataCards(customDataCards)
        self.um_pre_op_LogFile = self.mcnpInputFileName + "_um.log"
        um_file_stream = open(self.um_pre_op_LogFile, 'w')
        subprocess.call(['um_pre_op', '-m', '-dc', str(self.dataCardFile),
                         '-o', str(self.mcnpInputFileName),
                         str(self.gmshAsm.inpFileName)], stdout=um_file_stream)
        um_file_stream.close()
        self.mcnpInput = fileToList(self.mcnpInputFileName)
        del self.mcnpInput[0]  # Remove garbage at begining of this file?

    def modelBounds(self):
        pre_op_output = fileToList(self.um_pre_op_LogFile)
        gL = None
        for i, line in enumerate(pre_op_output):
            if line.strip() == "Global Model Extents":
                gL = i
                break
        # Formatted: minx, maxx, miny, maxy, minz, maxz
        meshBounds = []
        if gL:
            for line in pre_op_output[i + 1: i + 4]:
                meshBounds.append(float(line.split()[2]) - 1e-4)
                meshBounds.append(float(line.split()[5]) + 1e-4)
            meshBounds = np.array(meshBounds)
        else:
            meshBounds = None
            print("Global model Extents not found")
        return meshBounds

    def parseMCout(self):
        """ Grabs keff from mcnp output file """
        keffKey = re.compile('\s\s+col/abs/trk')
        self.keff = None
        with open(self.mcnpOutputFileName + 'o') as f:
            lines = f.readlines()
            for line in lines:
                if keffKey.match(line):
                    self.keff = float(line.split()[2])
                    break
        return self.keff

    def runMCNP(self):
        """ Backup previous run outputs then run MCNP """
        try:
            subprocess.call(['mv', str(self.mcnpOutputFileName) + 'o', str(self.mcnpOutputFileName) + 'o.bak'])
            subprocess.call(['mv', str(self.mcnpOutputFileName) + 's', str(self.mcnpOutputFileName) + 's.bak'])
            subprocess.call(['mv', str(self.mcnpOutputFileName) + 'r', str(self.mcnpOutputFileName) + 'r.bak'])
        except:
            pass
        try:
            if self.srctpFileName:
                subprocess.call(['mcnp6', 'i=' + str(self.mcnpInputFileName), 'n=' + str(self.mcnpOutputFileName),
                                 'n=' + str(self.mcnpOutputFileName), 'srctp=', str(self.srctpFileName) + '.bak', 'tasks ' + str(self.cores)])
            else:
                subprocess.call(['mcnp6', 'i=' + str(self.mcnpInputFileName), 'n=' + str(self.mcnpOutputFileName), 'tasks ' + str(self.cores)])
        except:
            # MPI EXE
            if self.srctpFileName:
                subprocess.call(['mpiexec', '-np', str(self.cores), 'mcnp6.mpi', 'i=' + str(self.mcnpInputFileName),
                                 'n=' + str(self.mcnpOutputFileName), 'srctp=', str(self.srctpFileName) + '.bak'])
            else:
                subprocess.call(['mpiexec', '-np', str(self.cores), 'mcnp6.mpi', 'i=' + str(self.mcnpInputFileName), 'n=' + str(self.mcnpOutputFileName)])
        self.mcnpIters += 1


if __name__ == "__main__":
    """ Test mcnp driver """
    # create basic block geometry
    # Fuel
    fuelBlocks = []
    dz = 10
    dx = 0.5
    fuelzmax = 100
    fuelxmax = 1
    # Block origins
    z0s = np.arange(0, fuelzmax, dz)
    x0s = np.arange(0, fuelxmax, dx)
    y0s = np.array([0])
    i = 1
    for x0 in x0s:
        for y0 in y0s:
            for z0 in z0s:
                tempCube = GMSHcube([x0, y0, z0], [dx, 1, dz], [5, 1, 5], source=True,
                                    cellID=i, matID=i, matName='21-fuel', density=-10.24)
                tempCube.injectParameters()
                tempCube.runPartGMSH()
                fuelBlocks.append(tempCube)
                i += 1
    # Moderator
    modBlocks = []
    dz = 10
    dx = 2
    modzmax = 100
    modxmax = fuelxmax + 2
    z0s = np.arange(0, modzmax, dz)
    x0s = np.arange(fuelxmax, modxmax, dx)
    y0s = np.array([0])
    for x0 in x0s:
        for y0 in y0s:
            for z0 in z0s:
                tempCube = GMSHcube([x0, y0, z0], [dx, 1, dz], [5, 1, 5], source=False,
                                    cellID=i, matID=i, matName='00-lwtr', density=-1.0)
                tempCube.injectParameters()
                tempCube.runPartGMSH()
                modBlocks.append(tempCube)
                i += 1
    # Top Moderator
    topMod = GMSHcube([0, 0, 100], [3, 1, 30], [5, 1, 5], source=False,
                      cellID=50, matID=50, matName='00-lwtr', density=-1.0)
    topMod.injectParameters()
    topMod.runPartGMSH()
    modBlocks.append(topMod)
    # Bottom Mod
    botMod = GMSHcube([0, 0, -30], [3, 1, 30], [5, 1, 5], source=False,
                      cellID=51, matID=51, matName='00-lwtr', density=-1.0)
    botMod.injectParameters()
    botMod.runPartGMSH()
    modBlocks.append(botMod)
    # fuelBlock = GMSHcube([0, 0, 0], [30, 30, 30], [10, 10, 10], source=True, cellID=1,
    #                      matID=1, matName='31-fuel', density=-10.24)
    # fuelBlock.injectParameters()
    # fuelBlock.runPartGMSH()
    # Build assembly
    myAsm = GMSHassembly(fuelBlocks + modBlocks)
    #myAsm = GMSHassembly([fuelBlock])
    myAsm.buildAssembly()
    myAsm.writeAsmINP()
    # build mcnp deck
    mcnpRun = MCNPdriver(myAsm)
    mcnpRun.runUmPreOp()
    mcnpRun.setBCs()
    mcnpRun.runMCNP()
    keff = mcnpRun.parseMCout()
    print("K-effective estimate:")
    print keff

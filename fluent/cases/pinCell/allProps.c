#include "udf.h"

// H2O PROPS at 2250 psia
DEFINE_PROPERTY(H2O_density, cell, thread)
{
    real t, rho;
    t = C_T(cell, thread);
    // w / m K
    if (t < 300.)
    {
        rho = 1003.4;
    }
    else if (t < 618.01)
    {
        rho = -6.95709e-8*pow(t,4.)+1.17907e-4*pow(t,3.)-7.61556e-2*pow(t,2.)+21.2884*t-1151.26;
    }
    else
    {
        rho = 594.13;
    }
    return rho;
}

DEFINE_PROPERTY(H2O_viscosity, cell, thread)
{
    real t, mu;
    t = C_T(cell, thread);
    // w / m K
    if (t < 380.)
    {
        mu = -9.09487e-10*pow(t, 3.)+1.01182e-6*pow(t, 2.)-3.78494e-4*t+0.0478904;
    }
    else
    {
        mu = 6.66161e-14*pow(t, 4.)-1.53465e-10*pow(t, 3.)+1.32487e-7*pow(t, 2.)-5.12276e-5*t+7.63282e-3;
    }
    return mu;
}

DEFINE_SPECIFIC_HEAT(H2O_cp, t, Tref, h, yi)
{
    real cp;
    // j / mol K  -> j / kg K
    if (t < 460.)
    {
        cp = 6.7314e-8*pow(t, 4.)-5.94628e-5*pow(t, 3.)+0.0191488*pow(t, 2.)-2.40736*t+4198.27;
    }
    else if (t < 618.01)
    {
        cp = 7.29113e-6*pow(t, 4.)-1.43767e-2*pow(t, 3.)+10.6559*pow(t, 2.)-3512.65*t+438300;
    }
    else
    {
        cp = 7.29113e-6*pow(618.01, 4.)-1.43767e-2*pow(618.01, 3.)+10.6559*pow(618.01, 2.)-3512.65*618.01+438300;
    }
    *h = cp*(t - Tref);
    return cp;
}

// UO2 PROPS
DEFINE_PROPERTY(UO2_conductivity, cell, thread)
{
    real t, k1;
    t = C_T(cell, thread) / 1000.;
    // w / m K
    k1 = 100. / (7.5408 + 17.692*t + 3.6142*pow(t, 2.)) + 6400. / pow(t, 5./2.) * exp(-16.35/t);
    return k1;
}

DEFINE_SPECIFIC_HEAT(UO2_cp, t, Tref, h, yi)
{
    real cp;
    real m;
    m = 270.0e-3;  // kg / mol
    // j / mol K  -> j / kg K
    cp = (1/m) * (52.1743 + 87.951*(t/1000.) - 84.2411*pow((t/1000.), 2.) + 31.542*pow((t/1000.), 3.) - 2.6334*pow((t/1000.), 4.) - 0.71391*pow((t/1000.), -2.));
    *h = cp*(t - Tref);
    return cp;
}

// ZIRC-2 PROPS
DEFINE_PROPERTY(zirc_conductivity, cell, thread)
{
    real t, k1;
    t = C_T(cell, thread);
    // w / m K
    k1 = 12.767 - 5.4348e-4*t + 8.9818e-6*pow(t, 2.);
    return k1;
}

DEFINE_SPECIFIC_HEAT(zirc_cp, t, Tref, h, yi)
{
    real cp;
    // j / kg K
    cp = 255.66 + 0.1024*t;
    *h = cp*(t - Tref);
    return cp;
}

// He PROPS
DEFINE_PROPERTY(he_conductivity, cell, thread)
{
    real t, k1;
    t = C_T(cell, thread);
    // w / m K
    // k1 = -5.84419e-8*pow(t, 2.) + 3.66895e-4*t + 5.38251e-2;
    k1 = 0.4769772;  // assuming gap of 0.0084 cm and h of 5678.3  W / m k
    return k1;
}

DEFINE_PROPERTY(he_density, cell, thread)
{
    real t, rho;
    t = C_T(cell, thread);
    // kg/m3
    rho = 1.27027e-7*pow(t, 2.) - 3.15058e-4*t + 0.230879;
    return rho;
}

DEFINE_SOURCE(vol_heat_source, cell, thread, dS, eqn)
{
	real source;
	source = C_UDSI(cell, thread, 0);
	return source;
}

// mesh size
cl1 = 0.001;
h = 3;
fw = 0.005;
sw = 0.005;

// points around fluid
Point(1) = {0, 0, 0, cl1};
Point(2) = {fw, 0, 0, cl1};
Point(3) = {fw, h, 0, cl1};
Point(4) = {0, h, 0, cl1};

// points around solid
Point(5) = {-sw, h, 0, cl1};
Point(6) = {-sw, 0, 0, cl1};

// lines and structured segregation scheme
Line(1) = {1, 2};
Transfinite Line {1} = 30 Using Progression 1.2;
Line(2) = {2, 3};
Transfinite Line {2} = 640 Using Progression 1;
Line(3) = {3, 4};
Transfinite Line {3} = 30 Using Progression 0.8;
Line(4) = {4, 1};
Transfinite Line {4} = 640 Using Progression 1;

// solid side lines
Line(5) = {4, 5};
Transfinite Line {5} = 10 Using Progression 1;
Line(6) = {5, 6};
Transfinite Line {6} = 640 Using Progression 1;
Line(7) = {6, 1};
Transfinite Line {7} = 10 Using Progression 1;

// line loops
Line Loop(6) = {2, 3, 4, 1};
Line Loop(7) = {-4, 5, 6, 7};

// surfaces construced from line loops
Plane Surface(6) = {6};
Plane Surface(7) = {7};

// Structured surfaces
Transfinite Surface {6} = {1, 2, 3, 4};
Transfinite Surface {7} = {1, 4, 5, 6};

// physical labels
Physical Line("wall") = {4};
Physical Line("sym") = {2};
Physical Line("outlet") = {3};
Physical Line("inlet") = {1};
Physical Line("solid-up") = {5};
Physical Line("solid-sym") = {6};
Physical Line("solid-dwn") = {7};
Physical Surface("fluid") = {6};
Physical Surface("solid") = {7};

Recombine Surface {6};
Recombine Surface {7};

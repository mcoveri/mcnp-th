Geometry.CopyMeshingMethod = 1;
// Mesh.SubdivisionAlgorithm = 2;
//
// CONSTANTS
ppitch = 0.0126;
fuelR = 0.004096;
cladI = 0.00418;
cladO = 0.00475;
h = 3.6576;
pz = 400;

// Gmsh project created on Thu Apr 30 18:40:53 2015
Point(1) = {0, 0, 0, 1.0};
Point(2) = {0, fuelR, 0, 1.0};
Point(3) = {fuelR, 0, 0, 1.0};
Point(4) = {-fuelR, 0, 0, 1.0};
Point(5) = {0, -fuelR, 0, 1.0};
Circle(1) = {3, 1, 2};
Circle(2) = {2, 1, 4};
Circle(3) = {4, 1, 5};
Circle(4) = {5, 1, 3};
Point(6) = {0, cladI, 0, 1.0};
Point(7) = {cladI, 0.0, 0, 1.0};
Point(8) = {0.0, -cladI, 0, 1.0};
Point(9) = {-cladI, 0.0, 0, 1.0};
Circle(5) = {7, 1, 6};
Circle(6) = {6, 1, 9};
Circle(7) = {9, 1, 8};
Circle(8) = {8, 1, 7};
Point(10) = {0, cladO, 0, 1.0};
Point(11) = {cladO, 0.0, 0, 1.0};
Point(12) = {0.0, -cladO, 0, 1.0};
Point(13) = {-cladO, 0., 0, 1.0};
Circle(9) = {11, 1, 10};
Circle(10) = {10, 1, 13};
Circle(11) = {13, 1, 12};
Circle(12) = {12, 1, 11};
Point(14) = {ppitch/2., ppitch/2., 0, 0.002};
Point(15) = {ppitch/2., -ppitch/2., 0, 0.002};
Point(16) = {-ppitch/2., -ppitch/2., 0, 0.002};
Point(17) = {-ppitch/2., ppitch/2., 0, 0.002};
Line(13) = {15, 14};
Line(14) = {14, 17};
Line(15) = {17, 16};
Line(16) = {16, 15};

// Begin line loops
// FUEL R1
Line Loop(1) = {1, 2, 3, 4};
// HE R2
Line Loop(2) = {5, 6, 7, 8};
// ZIRC R3
Line Loop(3) = {9, 10, 11, 12};
// PIN BOX
Line Loop(4) = {13, 14, 15, 16};

// 2D Surfaces
// FUEL
Plane Surface(1) = {1};
// HE
Plane Surface(2) = {1, 2};
// ZIRC
Plane Surface(3) = {2, 3};
// MOD
Plane Surface(4) = {3, 4};

// Begin Transfinite structured mesh params
// Fuel R1
Transfinite Line{1, 2, 3, 4} = 10 Using Progression 1;
// HE R2
Transfinite Line{5, 6, 7, 8} = 20 Using Progression 1;
// ZIR R3
Transfinite Line{9, 10, 11, 12} = 20 Using Progression 1;
// No transfinite in Mod region
//Transfinite Surface{1};
//Transfinite Surface{2};
//Transfinite Surface{3};

// Extrude and Recombine
// FUEL
Extrude {0, 0, h} { Surface{1}; Layers{pz}; Recombine; }
// HE
Extrude {0, 0, h} { Surface{2}; Layers{pz}; Recombine; }
// ZIRC
Extrude {0, 0, h} { Surface{3}; Layers{pz}; Recombine; }
// MOD
Extrude {0, 0, h} { Surface{4}; Layers{pz}; Recombine; }

// Create  Volumes
// FUEL
// Physical Volume("Fuel") = {1};
// HE
// Physical Volume("HE") = {2};
// ZIRC
// Physical Volume("Zirc") = {3};
// Mod
// Physical Volume("Mod") = {4};

nPins = 3;
pinWidth = nPins - 1;
For y In {0:ppitch*pinWidth:ppitch}
For x In {0:ppitch*pinWidth:ppitch}
If(!(x==0 && y==0))
newBases[] = Translate {x, y, 0.} { Duplicata{ Surface{1, 2, 3, 4}; } } ;
Extrude {0, 0, h} { Surface{newBases[]} ; Layers{pz}; Recombine; }
EndIf
EndFor
EndFor

// Create Volumes from coppied volumes
For j In {1:36:1}
Physical Volume(j) = {j};
EndFor

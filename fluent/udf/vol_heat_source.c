#include "udf.h"

DEFINE_SOURCE(vol_heat_source, cell, thread, dS, eqn)
{
	real source;
	source = C_UDSI(cell, thread, 0);
	return source;
}

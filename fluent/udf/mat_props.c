#include "udf.h"

// UO2 PROPS
DEFINE_PROPERTY(UO2_conductivity, cell, thread)
{
    real t, k1;
    t = C_T(cell, thread) / 1000.;
    // w / m K
    k1 = 100. / (7.5408 + 17.692*t + 3.6142*pow(t, 2.)) + 6400. / pow(t, 5./2.) * exp(-16.35/t);
    return k1;
}

DEFINE_SPECIFIC_HEAT(UO2_cp, t, Tref, h, yi)
{
    real cp;
    real m;
    m = 270.0e-3;  // kg / mol
    // j / mol K  -> j / kg K
    cp = (1/m) * (52.1743 + 87.951*(t/1000.) - 84.2411*pow((t/1000.), 2.) + 31.542*pow((t/1000.), 3.) - 2.6334*pow((t/1000.), 4.) - 0.71391*pow((t/1000.), -2.));
    *h = cp*(t - Tref);
    return cp;
}

// ZIRC-2 PROPS
DEFINE_PROPERTY(zirc_conductivity, cell, thread)
{
    real t, k1;
    t = C_T(cell, thread);
    // w / m K
    k1 = 12.767 - 5.4348e-4*t + 8.9818e-6*pow(t, 2.);
    return k1;
}

DEFINE_SPECIFIC_HEAT(zirc_cp, t, Tref, h, yi)
{
    real cp;
    // j / kg K
    cp = 255.66 + 0.1024*t;
    *h = cp*(t - Tref);
    return cp;
}

// He PROPS
DEFINE_PROPERTY(he_conductivity, cell, thread)
{
    real t, k1;
    t = C_T(cell, thread);
    // w / m K
    k1 = -5.84419e-8*pow(t, 2.) + 3.66895e-4*t + 5.38251e-2;
    return k1;
}

DEFINE_PROPERTY(he_density, cell, thread)
{
    real t, rho;
    t = C_T(cell, thread);
    // kg/m3
    rho = 1.27027e-7*pow(t, 2.) - 3.15058e-4*t + 0.230879;
    return rho;
}

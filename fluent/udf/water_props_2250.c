#include "udf.h"

// H2O PROPS at 2250 psia
DEFINE_PROPERTY(H2O_density, cell, thread)
{
    real t, rho;
    t = C_T(cell, thread);
    // w / m K
    if (t < 300.)
    {
        rho = 1003.4;
    }
    else if (t < 618.01)
    {
        rho = -6.95709e-8*pow(t,4.)+1.17907e-4*pow(t,3.)-7.61556e-2*pow(t,2.)+21.2884*t-1151.26;
    }
    else
    {
        rho = 594.13;
    }
    return rho;
}

DEFINE_PROPERTY(H2O_viscosity, cell, thread)
{
    real t, mu;
    t = C_T(cell, thread);
    // w / m K
    if (t < 380.)
    {
        mu = -9.09487e-10*pow(t, 3.)+1.01182e-6*pow(t, 2.)-3.78494e-4*t+0.0478904;
    }
    else
    {
        mu = 6.66161e-14*pow(t, 4.)-1.53465e-10*pow(t, 3.)+1.32487e-7*pow(t, 2.)-5.12276e-5*t+7.63282e-3;
    }
    return mu;
}

DEFINE_SPECIFIC_HEAT(H2O_cp, t, Tref, h, yi)
{
    real cp;
    // j / mol K  -> j / kg K
    if (t < 460.)
    {
        cp = 6.7314e-8*pow(t, 4.)-5.94628e-5*pow(t, 3.)+0.0191488*pow(t, 2.)-2.40736*t+4198.27;
    }
    else if (t < 618.01)
    {
        cp = 7.29113e-6*pow(t, 4.)-1.43767e-2*pow(t, 3.)+10.6559*pow(t, 2.)-3512.65*t+438300;
    }
    else
    {
        cp = 7.29113e-6*pow(618.01, 4.)-1.43767e-2*pow(618.01, 3.)+10.6559*pow(618.01, 2.)-3512.65*618.01+438300;
    }
    *h = cp*(t - Tref);
    return cp;
}

import os
import numpy as np
from gmshDriver import *
baseDir = '/data/wlg333/ms_thesis/mcnp-th/cases/unitcell/'


### PIN CELL TEST RUN ###
##############################
### GEOMETRY GENERATION FN ###
##############################
def geometrySetup(**kwargs):
    """ Setup quarter sym pin cell case geometry. """
    meshBaseDir = os.path.join(baseDir, "mesh/")
    fuelBlocks, zircBlocks, modBlocks = [], [], []
    fuelT = 570.
    modT = 570.
    dz = 10.0
    pz = 2
    zmax = 5.0
    ppitch = 1.260
    nPhi = 12  # azimuthal segments
    radCorrs = {4: 0.005708, 6: 0.002056, 9: 0.000803, 12: 0.0}  # radial correction factors
    radCorr = radCorrs.get(nPhi)
    fuelrmax = 0.4096
    hermax = 0.418
    zircrmax = 0.475
    # Correct radii due to discritization error
    fuelrmax += fuelrmax * radCorr
    hermax += hermax * radCorr
    zircrmax += zircrmax * radCorr
    z0s = np.arange(0, zmax, dz)
    x0s = np.array([0])
    y0s = np.array([0])
    quadrents = ['0', 'Pi / 2', 'Pi', 'Pi * (3 / 2)']
    i = 1
    for x0 in x0s:
        for y0 in y0s:
            for z0 in z0s:
                for quad in quadrents:
                    # fuel
                    tempWedge = GMSHwedge([x0, y0, z0, quad], [fuelrmax / 2., np.pi / 2, dz], [3, nPhi, pz],
                                          source=True, cellID=10000 + i, matID=10000 + i, matName={'21-fuel': 1.0},
                                          density=-10.24, temperature=fuelT,
                                          geoFile=os.path.join(meshBaseDir, "Part-" + str(10000 + i) + ".geo")
                                          )
                    tempWedge.injectParameters()
                    tempWedge.runPartGMSH()
                    fuelBlocks.append(tempWedge)
                    i += 1
                    # fuel 2
                    tempRing = GMSHring([x0, y0, z0, fuelrmax / 2., quad], [fuelrmax, np.pi / 2, dz], [3, nPhi, pz],
                                        source=True, cellID=10000 + i, matID=10000 + i, matName={'21-fuel': 1.0},
                                        density=-10.24, temperature=fuelT,
                                        geoFile=os.path.join(meshBaseDir, "Part-" + str(10000 + i) + ".geo")
                                        )
                    tempRing.injectParameters()
                    tempRing.runPartGMSH()
                    fuelBlocks.append(tempRing)
                    i += 1
                    # cladding
                    tempRing = GMSHring([x0, y0, z0, hermax, quad], [zircrmax, np.pi / 2, dz], [3, nPhi, 3],
                                        source=False, cellID=i, matID=i, matName={'02-zirc': 1.0},
                                        density=-6.56, temperature=modT,
                                        geoFile=os.path.join(meshBaseDir, "Part-" + str(i) + ".geo")
                                        )
                    tempRing.injectParameters()
                    tempRing.runPartGMSH()
                    zircBlocks.append(tempRing)
                    i += 1
                    # moderator octant sym blocks
                    tempOct = GMSHoctant([x0, y0, z0, zircrmax, quad], [ppitch, 'Pi / 4.', dz], [3, nPhi, 3],
                                         source=False, cellID=i, matID=i, matName={'00-lwtr': 1.0},
                                         density=-0.8, temperature=modT,
                                         geoFile=os.path.join(meshBaseDir, "Part-" + str(i) + ".geo")
                                         )
                    tempOct.injectParameters()
                    tempOct.runPartGMSH()
                    modBlocks.append(tempOct)
                    i += 1
                    tempOct = GMSHoctant([x0, y0, z0, zircrmax, quad + ' + Pi / 4.'], [ppitch, 'Pi / 4.', dz], [3, nPhi, 3],
                                         source=False, cellID=i, matID=i, matName={'00-lwtr': 1.0},
                                         density=-0.8, temperature=modT,
                                         geoFile=os.path.join(meshBaseDir, "Part-" + str(i) + ".geo")
                                         )
                    tempOct.injectParameters()
                    tempOct.runPartGMSH()
                    modBlocks.append(tempOct)
                    i += 1
    myAsm = GMSHassembly(fuelBlocks + zircBlocks + modBlocks, meshBaseDir, 'asm')
    myAsm.buildAssembly()
    myAsm.writeAsmINP(meshBaseDir=meshBaseDir)
    return myAsm

###########################
### MCNP PARAMETER DICT ###
###########################
MCNPparams = {'runName': 'unitCell',
              'mcnpBaseDir': os.path.join(baseDir, 'mcnp/'),
              'eeoutFile': os.path.join(baseDir, 'mesh/asm.eeout'),
              'bc_axl': 'ref',
              'cores': 23,
              'kcode': {0: '1e6 1.0 85 300'}
              }

#############################
### FLUENT PARAMETER DICT ###
#############################
fluentParams = {'fluentCasName': os.path.join(baseDir, 'fluent/cases/pinCell/pinCell.cas'),
                'templateJournal': os.path.join(baseDir, 'fluent/journals/template_journal.jou'),
                'THDim': 3,
                'THheatSource': {'solid_source_ip':
                                 [os.path.join(baseDir, 'fluent/cases/pinCell/pinCell_source_solid.ip'),
                                  (10000, 20000),
                                  'uds-0']
                                 },
                'THdensityData': os.path.join(baseDir, 'fluent/cases/pinCell/pinCell_density.out'),
                'THtempData': os.path.join(baseDir, 'fluent/cases/pinCell/pinCell_temperature.out'),
                'THiters': 600
                }

############################
### MESH TO MESH SETINGS ###
############################
meshParams = {'mcnp2TH_tr': [0., 0., 0.],   # shift MC f6 tally result to TH ordinates [cm]
              'mcnp2TH_reshape': [0, 1, 2],          # swap z coords with y coords
              'mcnp2TH_scale': 0.01,                 # convert cm to m
              'TH2mcnp_tr': [0., 0., 0.],         # shift TH result to MCNP grid ordinates [cm]
              'TH2mcnp_reshape': [0, 1, 2],          # swaps y coords with z coords
              'TH2mcnp_scale': 100                   # convert meters to cm
              }

############################
###    COUPLER SETINGS   ###
############################
couplerParams = {'outerIters': 0,
                 'coupled': False,
                 'qTol': 1e-2,      # vol heat gen field convergence tolorance
                 'tTol': 1e-2,      # temperature field convergence tolorance
                 'kTol': 50e-5,
                 'iRlx': 0,         # outer iteration at which to begin back-averaging heating tally
                 'maxBI': 2,
                 'minWI': 0.5,
                 'powerNorm': 0.067e6,  # Power normalization factor (asm avg vol heat source W/m^3)
                 'logDir': os.path.join(baseDir, 'logs/corVol/unitCell_DTC_2_570'),
                 'updateMask': [9998, 9999]  # cellIDs to ignore density and temperature updates
                 }

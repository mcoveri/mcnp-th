#!/usr/bin/python

# Generates MCNP meshes
# DEPRECIATED
#
# Author: William Gurecky
# Date: 2/25/2015
#
# Changelog
# ----------
#
# 2/25/2015     Begin construction on parent mesh class and plane wall case
# 6/01/2015     DEPRECIATED DOES NOTHING


import numpy as np
import sys


class MCNPcell(object):
    """ Building block of MCNP mesh.
    Stores cell temperature, material, geometry and location params.
    T in Kelvin
    """
    def __init__(self, cellID, matID, materialTag, T, geomType='RPP'):
        self.cellID = cellID  # cell ID used in mcnp deck
        self.matID = matID    # Shows up as material ID used in mcnp deck
        self.materialTag = materialTag
        self.geomType = geomType
        self.cellT = T
        self.computeT()
        self.tallyID = str(cellID) + "4"

    def setGeom(self):
        pass

    def computeT(self):
        """ Free gas thermal temperature of cell. """
        self.freeGasT = self.cellT * 8.617e-11

    def pseudoMaterial(self):
        """ Interpolates known materials in a supplied cross section material
        database to generate a pseudo material that weights the constituent
        material cross sections by inverse distance to the specified cell
        Temperature """
        massfrac = 10.  # placeholder
        self.materials = [('zaid', massfrac), ('zaid', massfrac)]
        self.materialDensity = 10.

    def printCell(self):
        return str(self.cellID) + " " + str(self.matID) + str(self.materialDensity) + " " + str(self.bBoolString) + \
            ' T=' + str(self.freeGasT) + "imp:n=1"

    def printMatCard(self):
        matBlock = "M" + str(self.matID) + "    " + str(self.materials[0][0]) + " " + str(self.materials[0][1])
        for mat in self.materials[1:]:
            matBlock += "     " + str(mat[0]) + " " + str(mat[1])
        return matBlock

    def printF4tally(self):
        f4String = "F" + self.tallyID + ":N " + str(self.cellID) + " \n"
        fmString = "FM" + self.tallyID + "-1 1 -6"
        return f4String + fmString


class RPPcell(MCNPcell):
    """ Generic RPP macrobody MCNP cell. """
    def __init__(self, boundingPlaneIDs, **kwargs):
        super(RPPcell, self).__init__(**kwargs)
        if len(boundingPlaneIDs) != 6:
            sys.exit("Error.  A box requires 6 sides.")
        self.boundingPlaneIDs = boundingPlaneIDs
        self.__computeInside()

    def __computeInside(self):
        westFace = str(self.boundingPlaneIDs[0])
        eastFace = '-' + str(self.boundingPlaneIDs[1])
        backFace = str(self.boundingPlaneIDs[2])
        frontFace = '-' + str(self.boundingPlaneIDs[3])
        southFace = str(self.boundingPlaneIDs[4])
        northFace = '-' + str(self.boundingPlaneIDs[5])
        self.bFaces = [westFace, southFace, eastFace, northFace, backFace, frontFace]
        self.bBoolString = ' '.join(self.bFaces)


class MCNPmesh(object):
    """ MCNP mesh class.  Collects MCNP cells.  Can write mesh out to
    skeleton MCNP deck.
    """
    def __init__(self, meshType='planeWall'):
        self.cells = {}

    def overlayMeshTal(self, origin, divs, **kwargs):
        """ Places a mesh tally in MCNP deck.  Supply number of x y and z divisions.
        divs [nx, ny, nz]
        Origin [x, y, z] coordinates of bottom left corner.
        """
        pass

    def writeToMCNP(self):
        pass


class PlaneWallMesh(MCNPmesh):
    """ Specific use case for planar wall mesh.
    """
    def __init__(self, mins, maxs, divs, boundConds=["*", "", "*", "*", "", ""]):
        planeID = 1  # Starting planeID
        self.coords = [np.linspace(mins[0], maxs[0], divs[0]),
                       np.linspace(mins[1], maxs[1], divs[1]),
                       np.linspace(mins[2], maxs[2], divs[2])]
        planes = [None, None, None]
        directionIterate = ['PX', 'PY', 'PZ']
        for i, normalDirection in enumerate(directionIterate):
            planes[i], planeID = self.__createPlanes(planeID, normalDirection, self.coords[i], boundConds[2*i:2*i+2])
        self.planes = dict(planes[0].items() + planes[1].items() + planes[2].items())
        self.makeCells()

    def __createPlanes(planeID, normD, xcoords, boundConds):
        for i, x in enumerate(xcoords):
            if x == max(xcoords):
                # set reflective flag
                refFlag = boundConds[1]
            elif x == min(xcoords):
                refFlag = boundConds[0]
            else:
                refFlag = ""
            xplanes[planeID] = {'planeType': normD, 'planeCoord': x, 'bc': refFlag}
            planeID += 1
        return xplanes, planeID

    def __getPlanePairs(self):
        """ Finds the nearest neighbor plane.  Each plane is given top and bottom neighbors.
        None in case of boundary planes. """
        pass

    def makeCells(self):
        for k, z in enumerate(zplanes):
            boundingZPlanes = [zplanes[k], zplanes[k+1]]
            boundingZplanes = self.planes[2][z]
            for i, x in enumerate(xplanes):
                boundingXPlanes = [xplanes[i], xplanes[i+1]]
                for y, j in enumerate(yplanes):
                    boundingYPlanes = [yplanes[j], yplanes[j+1]]
                    boundPlanes = boundingXPlanes + boundingYPlanes + boundingZPlanes
                    cellID += 1

    def writeMeshToMCNP(self):
        self.geomBlock = "c MCNP MESH GENERATED BY PLANEMESH CLASS \n "
        for planeID, planeData in self.planes.iteritems():
            self.geomBlock += planeData['bc'] + str(planeID) + " " + str(planeData['planeType']) + \
                " " + str(planeData['planeCoord']) + " \n"



def main():
    pass

if __name__ == "__main__":
    main()

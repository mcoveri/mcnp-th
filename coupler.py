#!/usr/bin/python

# Provides MCNP tally to FLUENT interpolation file routines
# and FLUENT temperature map to MCNP mesh routines
#
# Author: William Gurecky
# Date: 2/25/2015
#
# Changelog
# ----------
#
#
import argparse
import sys
import time
import numpy as np
from gmshDriver import *
import mcnpDriver
import fluentDriver
import meshData
import os
dir = os.path.dirname(os.path.abspath(__file__))

try:
    import dill as pickle
    DILL = True
except:
    print("Warning: The python package Dill is not installed.  Pickled restart functionality is disabled.")
    DILL = False


class CoupledMCFL(object):
    """
    Build a complete MCNP input deck from geometry input.
    Stores outputs from mcnp and fluent for plotting and convergence checks.
    Requires an existing Fluent case setup.  The class does *NOT* set
    boundary conditions or material properties in fluent.  This must be done
    mannualy.
    """
    def __init__(self, geomGenerator, _inputDeck, **kwargs):
        # Load dictionaries which contain driver settings
        self.couplerParams = kwargs.pop('couplerParams', _inputDeck.couplerParams)
        self.meshParams = kwargs.pop('meshParams', _inputDeck.meshParams)
        self.fluentParams = kwargs.pop('fluentParams', _inputDeck.fluentParams)
        self.MCNPparams = kwargs.pop('MCNPparams', _inputDeck.MCNPparams)
        self.logBaseDir = self.couplerParams.pop('logDir', 'logs')
        geomArgs = kwargs.pop('geomArgs', None)
        self.kResult, self.tempResult, self.qResult, self.rhoResult = [0.0], [], [], []
        self.tempNorm, self.powerNorm = [1e10], [1e10]
        self.thTime, self.mcnpTime, self.overheadTime = 0, 0, 0
        self.converged = [False, False, False]
        self.maxItersStop = False
        self.i = 0  # iteration counter
        if hasattr(geomGenerator, '__call__'):
            self.runAsm = geomGenerator(myargs=geomArgs)  # pass any optional arguments to geometry generation fn
        else:
            self.runAsm = geomGenerator  # can also specify a GMSH assembly class from some other source
        # Initilize mcnp driver
        self.mcnp = mcnpDriver.MCNPdriver(self.runAsm, **self.MCNPparams)

    def runMC(self):
        """ Setup and execute MCNP run """
        if self.i in self.MCNPparams['kcode'].keys():
            self.mcnp.updateKcode(self.MCNPparams['kcode'][self.i])
        self.runAsm.buildAssembly()
        self.runAsm.writeAsmINP()  # these
        self.mcnp.runUmPreOp(self.MCNPparams.get('dataCards'))
        self.mcnp.setBCs(self.MCNPparams.get('bc_axl'))
        mcTimeStart = time.time()
        self.mcnp.runMCNP()
        self.mcnpTime = time.time() - mcTimeStart
        self.__collectMCOutput()

    def setMCNPcellT(self):
        """ Set temperature in each cell.  Requires Fluent temperature field data.
        Ignore cells that are flagged by the updateMask.
        """
        self.overTimeStart = time.time()
        for cell in self.runAsm.parts:
            if cell.cellID not in self.couplerParams['updateMask']:
                self.tempField.setCellProp(cell, 'temperature')

    def setMCNPcellRho(self):
        """ Only modify water densities in the problem.
        TODO: let material names to be update be a use set parameter.
        """
        for cell in self.runAsm.parts:
            if any('lwtr' in mat for mat in cell.materialName.keys()) \
                    and cell.cellID not in self.couplerParams['updateMask']:
                self.rhoField.setCellProp(cell, 'density')
            else:
                # Do not alter density of solid materials
                pass
        self.overheadTime = time.time() - self.overTimeStart

    def __collectMCOutput(self):
        """ Store keff, F6 tally data """
        self.kResult.append(self.mcnp.parseMCout())
        self.f6Tally = meshData.MeshData(self.MCNPparams['eeoutFile'], 'mcnp')
        # Make field data translations and scalings
        self.meshTransforms(self.f6Tally,
                            self.meshParams['mcnp2TH_scale'],
                            self.meshParams['mcnp2TH_tr'],
                            self.meshParams['mcnp2TH_reshape'],
                            self.couplerParams['powerNorm'],
                            'sum')
        if self.i > self.couplerParams['iRlx']:
            self.backAverageQ(self.couplerParams['iRlx'],
                              self.couplerParams['maxBI'],
                              self.couplerParams['minWI'])
        for key, value in self.fluentParams['THheatSource'].iteritems():
            self.f6Tally.writeToIP(value[2], ipFileName=value[0], matCr=value[1])
        self.qResult.append(self.f6Tally)

    def backAverageQ(self, iRlx, maxBackI=3, minWeight=0.5):
        '''
        Average f6 previous heating tally results into current solution.
        This averaging process reduces monte-carlo jitter and improves
        convergence characteristics.  Similar to under relaxation, but
        can incorperate several past iteration's into the leading solution vector.
        '''
        distanceFromIRlx = min(self.i + 1 - iRlx, maxBackI)
        weights = np.linspace(minWeight, 1, distanceFromIRlx)
        weights /= np.sum(weights)  # Norm weights so sum == 1.0
        if distanceFromIRlx > 1:
            previousTallies = np.array([previousTally.scalarField for previousTally in self.qResult[-distanceFromIRlx + 1:]])
            self.f6Tally.scalarField[-1] = np.sum(np.append(weights[-distanceFromIRlx:-1][:, None, None] * previousTallies,
                                                  np.array([weights[-1] * self.f6Tally.scalarField]), axis=0), axis=0)[-1]
        else:
            pass

    def backAverageVec(self, vecs, iRlx, maxBackI=3, minWeight=0.5):
        """
        Generalized version of backAverageQ.  Can take any list of vectors and
        apply the under relaxation desired. Used to under relax temperature and density fields.
        """
        distanceFromIRlx = min(self.i - iRlx, maxBackI)
        weights = np.linspace(minWeight, 1, distanceFromIRlx)
        weights /= np.sum(weights)
        if distanceFromIRlx > 1:
            relaxedVector = np.sum(np.append(weights[-distanceFromIRlx:-1][:, None] * vecs[-distanceFromIRlx:-1],
                                   np.array([weights[-1] * vecs[-1]]), axis=0), axis=0)
            return relaxedVector
        else:
            return vecs[-1]

    def meshTransforms(self, myMeshData, scale, tr, reshape, norm=1.0, preNorm=False):
        """ Translate and/or scale scalar field data """
        myMeshData.reshapeCoords(reshape)  # swap axes
        myMeshData.scaleCoords(scale)      # scale coordinate vals
        myMeshData.shiftCoords(tr)         # translate mesh
        myMeshData.normScalarField(norm, preNorm)   # normalize field vals

    def runTH(self, reRun=False):
        """ Execute Fluent """
        self.fluent = fluentDriver.FluentDriver(self.fluentParams['templateJournal'],
                                                dim=self.fluentParams['THDim'],
                                                cas=self.fluentParams['fluentCasName'],
                                                thIters=self.fluentParams['THiters'],
                                                tempData=self.fluentParams['THtempData'],
                                                densityData=self.fluentParams['THdensityData'],
                                                heatSource=self.fluentParams['THheatSource'])
        self.fluent.writeJournalFile()
        thTimeStart = time.time()
        self.fluent.runFluent()
        self.thTime = time.time() - thTimeStart
        if self.thTime < 40.0 and reRun is False:
            # RANT TIME.  OK, the license server (which is not under my
            # control) sometimes does not have any
            # HPC fluent licenses left because of either misconfiguation or too
            # many users are requesting HPC licenses at once.
            print("TH solve took less than 40 seconds.  Something went wrong in TH solver")
            print("Rerunning TH in 5 mins.")
            time.sleep(60 * 5)
            self.runTH(reRun=True)
        else:
            self.__collectTHOutput()
            self.i += 1
            if self.i >= self.couplerParams['outerIters']:
                self.maxItersStop = True

    def __collectTHOutput(self):
        """ Store T field data """
        self.tempField = meshData.MeshData(self.fluentParams['THtempData'], 'fluent-2')
        self.meshTransforms(self.tempField,
                            self.meshParams['TH2mcnp_scale'],
                            self.meshParams['TH2mcnp_tr'],
                            self.meshParams['TH2mcnp_reshape'])
        self.rhoField = meshData.MeshData(self.fluentParams['THdensityData'], 'fluent-2')
        self.meshTransforms(self.rhoField,
                            self.meshParams['TH2mcnp_scale'],
                            self.meshParams['TH2mcnp_tr'],
                            self.meshParams['TH2mcnp_reshape'],
                            1.0 / 1000.0)  # convert kg/m^3 (TH) to g/cc (mcnp)
        self.tempResult.append(self.tempField)
        self.rhoResult.append(self.rhoField)

    def checkConvergence(self):
        self.tempNorm.append(np.linalg.norm((self.tempResult[-1].scalarField[-1] - self.tempResult[-2].scalarField[-1])
                                            / np.mean(self.tempResult[-1].scalarField[-1])))
        self.powerNorm.append(np.linalg.norm((self.qResult[-1].scalarField[-1] - self.qResult[-2].scalarField[-1])
                                             / (self.qResult[-1].scalarField[-1])))
        if self.tempNorm[-1] <= self.couplerParams['tTol']:
            self.converged[0] = True
        if self.powerNorm[-1] <= self.couplerParams['qTol']:
            self.converged[1] = True
        if abs(self.kResult[-2] - self.kResult[-1]) <= self.couplerParams['kTol']:
            self.converged[2] = True

    def printInfo(self):
        """ Prints iteration results """
        statusLines = []
        statusLines.append("------------------- ITERATION: %i ---------------------\n" % (self.i))
        statusLines.append("keff                qNorm              tNorm\n")
        statusLines.append("------------------|-------------------|----------------\n")
        statusLines.append("%f           %e          %e         \n" % (self.kResult[-1], self.powerNorm[-1], self.tempNorm[-1]))
        statusLines.append("-------------------------------------------------------\n")
        statusLines.append("MCNP RUNTIME (s): %f ,   TH RUNTIME (s): %f \n" % (self.mcnpTime, self.thTime))
        statusLines.append("OVERHEAD TIME (s): %f " % (self.overheadTime))
        for line in statusLines:
            print(line)
        return statusLines

    def writeData(self, coupled=True):
        """ Write all field data, keff, and residuals to file for post processing. """
        # keff and resid log
        kLogFile = self.logBaseDir + '/keff_' + str(self.i) + '.log'
        listToFile(kLogFile, self.printInfo())
        if coupled:
            # temperature field
            self.tempField.writeScalarFieldToFile(self.logBaseDir + '/T_' + str(self.i) + '.log')
            # density field
            self.rhoField.writeScalarFieldToFile(self.logBaseDir + '/Rho_' + str(self.i) + '.log')
        # heat deposition tally
        self.f6Tally.writeScalarFieldToFile(self.logBaseDir + '/Q_' + str(self.i) + '.log')


def pickleState(coupledRunClassInstance, restartFileName):
    ''' Serialize state of coupled run for reloading '''
    with open(restartFileName, 'wb') as outFile:
        pickle.dump(coupledRunClassInstance, outFile)


def loadState(restartFileName):
    return pickle.load(open(restartFileName, 'rb'))


def main():
    """
    Load input file module.  Run coupled iterations untill convergence.
    """
    parser = argparse.ArgumentParser(description='MCNP - FLUENT coupling script.')
    parser.add_argument('-i', type=str, help='Python input deck.')
    parser.add_argument('-x', type=bool, help='0 or false for neutronics only.  Coupled MCNP-TH by default.')
    parser.add_argument('-r', type=str, help='Restart file to read in.')
    args = parser.parse_args()
    inputFile = args.i
    if args.r:
        restartFileIn = args.r
    else:
        restartFileIn = None
    try:
        _inputDeck = __import__("cases.%s" % inputFile, fromlist=["cases"])
        coupled = _inputDeck.couplerParams.pop('coupled', True)
        restartFileOut = _inputDeck.couplerParams.pop('restartFile', 'restart.pickle')
    except ImportError:
        sys.exit("Please supply valid python input file.")
    if args.x:
        coupled = args.x
    converged = False

    if DILL is True and restartFileIn:
        coupledRun = loadState(restartFileIn)
    else:
        coupledRun = CoupledMCFL(_inputDeck.geometrySetup, _inputDeck)
    while not converged:
        # Run MCNP
        coupledRun.runMC()
        # Run Fluent
        if coupled:
            coupledRun.runTH()
        # Check Convergence
        if coupledRun.i >= 2 and coupled:
            coupledRun.checkConvergence()
            if np.alltrue(coupledRun.converged):
                converged = True
        # Update MCNP cell temperatures and densities
        if coupled:
            coupledRun.setMCNPcellT()
            coupledRun.setMCNPcellRho()
        # Store field data for post processing
        coupledRun.writeData(coupled)
        if coupledRun.maxItersStop or not coupled:
            converged = True
        # Write restart file if iteration matches specified iter to dump
        if 'restartWrite' in _inputDeck.couplerParams.keys() and DILL is True:
            if coupledRun.i in _inputDeck.couplerParams['restartWrite']:
                pickleState(coupledRun, restartFileOut)

if __name__ == "__main__":
    main()

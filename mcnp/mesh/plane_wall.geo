cl__1 = 0.01;
Geometry.CopyMeshingMethod = 1;

// Note: Default GMSH units in meters
wFuel = 0.01;  // Half width of fuel region
wFluid = 0.02;  // Half width of fluid channel
height = 1.0;  // Axial height of fuel region
depth = 0.01;  // plane wall depth
NaxialEdits = 12;  // Number of axial fuel segments
NRFuelZones = 2;  // Number of radial fuel zones
zBlockHeight = height / NaxialEdits;

// 
// Fuel Box Geometry
Point(1) = {0, 0, 0, 1};
Point(2) = {wFuel / NRFuelZones, 0, 0, 1};
Point(3) = {wFuel / NRFuelZones, depth, 0, 1};
Point(4) = {0, depth, 0, 1};

// Translated points
Point(5) = {0, depth, zBlockHeight, 1};
Point(6) = {0, 0, zBlockHeight, 1};
Point(10) = {wFuel / NRFuelZones, 0, zBlockHeight, 1};
Point(14) = {wFuel / NRFuelZones, depth, zBlockHeight, 1};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line(8) = {5, 6};
Line(9) = {6, 10};
Line(10) = {10, 14};
Line(11) = {14, 5};
Line(13) = {4, 5};
Line(14) = {1, 6};
Line(18) = {2, 10};
Line(22) = {3, 14};
Line Loop(6) = {4, 1, 2, 3};
Plane Surface(6) = {6};
Line Loop(15) = {4, 14, -8, -13};
Ruled Surface(15) = {15};
Line Loop(19) = {1, 18, -9, -14};
Ruled Surface(19) = {19};
Line Loop(23) = {2, 22, -10, -18};
Ruled Surface(23) = {23};
Line Loop(27) = {3, 13, -11, -22};
Ruled Surface(27) = {27};
Line Loop(28) = {8, 9, 10, 11};
Plane Surface(28) = {28};
Surface Loop(1) = {6, 28, 15, 19, 23, 27};
Volume(1) = {1};

// Define structured mesh
Transfinite Line {9} = 10 Using Progression 1;
Transfinite Line {10} = 10 Using Progression 1;
Transfinite Line {11} = 10 Using Progression 1;
Transfinite Line {8} = 10 Using Progression 1;
Transfinite Line {18} = 10 Using Progression 1;
Transfinite Line {22} = 10 Using Progression 1;
Transfinite Line {13} = 10 Using Progression 1;
Transfinite Line {14} = 10 Using Progression 1;
Transfinite Line {1} = 10 Using Progression 1;
Transfinite Line {2} = 10 Using Progression 1;
Transfinite Line {3} = 10 Using Progression 1;
Transfinite Line {4} = 10 Using Progression 1;
Transfinite Surface {28};
Transfinite Surface {23};
Transfinite Surface {6};
Transfinite Surface {15};
Transfinite Surface {19};
Transfinite Surface {27};
Recombine Surface {15, 28, 23, 6, 19, 27};
Transfinite Volume{1} = {6, 10, 14, 5, 1, 2, 3, 4};
Translate {wFuel / NRFuelZones, 0, 0} {
  Duplicata { Volume{1}; }
}

// Volume 1, 29, ...
// += 27
// Duplicate fuel elements in z direction
For k In {1:NaxialEdits-1:1}
Translate {0, 0, k * zBlockHeight} {
  Duplicata { Volume{1}; }
}
Translate {0, 0, k * zBlockHeight} {
  Duplicata { Volume{29}; }
}
EndFor

// Label physical volumes according to AAAAAAA_ZZZ format
// Where AAAAAA is 'material'  and ZZZ if material ID used in mcnp
Physical Volume("material_100") = {1};
Physical Volume("material_101") = {29};
Physical Volume("material_102") = {56};
Physical Volume("material_103") = {83};
Physical Volume("material_104") = {110};
Physical Volume("material_105") = {137};
Physical Volume("material_106") = {164};
Physical Volume("material_107") = {191};
Physical Volume("material_108") = {218};
Physical Volume("material_109") = {245};
Physical Volume("material_110") = {272};
Physical Volume("material_111") = {299};
Physical Volume("material_112") = {326};
Physical Volume("material_113") = {353};
Physical Volume("material_114") = {380};
Physical Volume("material_115") = {407};
Physical Volume("material_116") = {434};
Physical Volume("material_117") = {461};
Physical Volume("material_118") = {488};
Physical Volume("material_119") = {515};
Physical Volume("material_120") = {542};
Physical Volume("material_121") = {569};
Physical Volume("material_122") = {596};
Physical Volume("material_123") = {623};

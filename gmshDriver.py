#!/usr/bin/python

#################           GMSH DRIVER              ################
#
# Constructs meshed parts to be incorperated into a single assembly
# Run GMSH to generate abaqus part template files.  Restructure
# part templates into MCNP- unstructured mesh compatible file.
# Assign cellID and Material to each gmsh part.
#
# Changelog
# ---------
#
# Author
# ------
# William Gurecky
# william.gurecky@utexas.edu
#
#####################################################################

import numpy as np
import subprocess
import sys
import re
import os
dir = os.path.dirname(os.path.abspath(__file__))


class GMSHpart(object):
    """
    Generic GMSH part class.  Contains basic cell info that spans all
    cell types.  Provides methods to run templeted GMSH.geo files
    through GMSH to produce abaqus.inp output files.
    """
    def __init__(self, **kwargs):
        """
        Initilize cell attribute dict.
        cell Attributes:
            matID - material identifcation number used in MCNP M cards
            cellID - for GMSH internal use.  cellID linked to unique matID
            sourceFlag - True if multiplying medium, false otherwise.
            materialName - material name dictionary {"mat-string": weight frac}
            density - g/cc of material
            temperature - in Kelvin
            source - indicates the region is multiplying or not. Used for initial MCNP neutron source guess.
            inpFileName - abaqus INP file name for part
            geoFile - GMSH .geo file name for part
        """
        self.cellID = kwargs.pop('cellID', 01)
        self.matID = kwargs.pop('matID', 01)
        self.source = kwargs.pop('source', False)
        self.materialName = kwargs.pop('matName', {'00-lwtr': 1.0})  # see materialDictionary for valid material names
        self.density = kwargs.pop('density', -0.8)
        self.temperature = kwargs.pop('temperature', 520.)
        self.geoFile = kwargs.pop('geoFile', os.path.join(dir, "meshes/Part-" + str(self.cellID) + ".geo"))
        self.inpFileName = kwargs.pop('inpFile', self.geoFile + ".inp")
        self.nodes = []
        self.elements = []

    def setTemp(self, T):
        self.temperature = T

    def runPartGMSH(self, w=0, forFluent=False):
        """
        Executes GMSH for a single part file.
        Edit the resulting INP output file so that it is either MCNP or FLUENT
        compatible, depending on the forFluent boolean setting.
        """
        subprocess.call(['gmsh', str(self.geoFile), '-3', '-o', self.inpFileName, '-v', '0'])
        self.inpFL = fileToList(self.inpFileName)
        if forFluent:
            return self.fixGMSHpartForFluent(w)
        else:
            self.fixGMSHpart()

    def writePartINP(self):
        """
        abaqus inp to file.
        """
        listToFile(self.inpFileName, self.inpFL)

    def getINP(self):
        """
        Returns contents of inp file in list format.
        GMSHassembly class builds completed ASM inp file by calling this
        method for every GMSHpart in the assembly.
        """
        return self.inpFL

    def fixGMSHpart(self):
        """
        Fixes some issues with GMSH generated .inp file so that it is
        MCNP compatible.
        Re-number all element IDs (elements must be labeled from 1-N)
        Add generator keyword and proper generator inputs e.g.:
            *ELSET, name="block_01", generate
            1, N, 1
        -----------
        Pseudo code
        -----------
        Strip ELSET from *Element line
        Modify Element IDs, change first col to 1-N
        Rewrite *ELSET line(s):
          Make "material_matID" elset
          Make "statistic_matID" elset
          If Source flag:
              Make "source_matID" elset
        Add generator inputs to *ELSETs
        Wrap with *Part .... *END Part keywords
        """
        # Add *Part keyword
        del self.inpFL[0]
        self.inpFL[0] = "*Part, name=Part-" + str(self.cellID) + "\n"
        #
        # Look for keywords
        reFlags = {'Node': "\*Node",
                   'Elm': "\*Element.*(C3.+,)",
                   'ELSET': "\*ELSET"}
        for key, val in reFlags.iteritems():
            reFlags[key] = re.compile(val)
        #
        # Find locations of keywords in file
        # WARNING: there can be multiple instances of the *ELEMENT keyword
        # due to different mesh element types: tets are C3D6 octs are C3D8
        # MCNP does not allow multiple mesh element types within the same part!
        flaggedDict = {}
        for i, line in enumerate(self.inpFL):
            check = checkLine(line, reFlags)
            if check:
                flaggedDict[check[0]] = {'i': i, 'match': check[1]}
                if check[0] == 'Elm':
                    self.elmType = check[1].group(1).strip(',')
            else:
                pass
        # ROUND NODES COORDINATES. MCNP PARSER BREAKS IF TINY FLOATING PT
        # ERRS ARE PRESENT IN NODE PLACEMENTS.
        nodeDefLineStart = flaggedDict['Node']['i'] + 1
        nodeDefLineEnd = flaggedDict['Elm']['i']
        fixedNodeLines = []
        for j, line in enumerate(self.inpFL[nodeDefLineStart: nodeDefLineEnd]):
            words = line.split()
            # fix
            for k, word in enumerate(words[1:]):
                words[k + 1] = '%.10e' % round(float(word.strip(',')), 10)
                if (k + 1) < 3:
                    words[k + 1] += ','
            fixedNodeLines.append('  '.join(words) + ' \n')
            self.nodes.append([float(x.strip(', ')) for x in words])
        self.nodes = np.array(self.nodes)
        self.inpFL[nodeDefLineStart: nodeDefLineEnd] = fixedNodeLines
        #
        # *Element line
        self.inpFL[flaggedDict['Elm']['i']] = "*Element, type=" + self.elmType + " \n"
        #
        # Modify Element IDs
        elementDefLineStart = flaggedDict['Elm']['i'] + 1
        #elementDefLineEnd = flaggedDict['ELSET']['i'] + 1  # BUGFIX
        elementDefLineEnd = flaggedDict['ELSET']['i']
        fixedEleLines = []
        for j, line in enumerate(self.inpFL[elementDefLineStart: elementDefLineEnd]):
            words = line.split()
            words[0] = str(j + 1) + ', '
            fixedEleLines.append('  '.join(words) + ' \n')
            self.elements.append([int(x.strip(', ')) for x in words])
        self.elements = np.array(self.elements)
        self.inpFL[elementDefLineStart: elementDefLineEnd] = fixedEleLines
        #
        # Remove unnecisary ELSET lines, replace with material, statistic, and
        # source ELSETs
        del self.inpFL[flaggedDict['ELSET']['i'] + 1:]
        #
        # Generate material elset
        self.inpFL[flaggedDict['ELSET']['i']] = "*Elset, elset=Set-material_" + str(self.matID) + ", generate" + "\n"
        self.inpFL.append("1, " + str(j + 1) + ", 1 \n")
        #
        # Generate statistic elset
        self.inpFL.append("*Elset, elset=Set-statistic_" + str(self.matID) + ", generate \n")
        self.inpFL.append("1, " + str(j + 1) + ", 1 \n")
        #
        # If source, generate source elset
        if self.source:
            self.inpFL.append("*Elset, elset=Set-source_" + str(self.matID) + ", generate \n")
            self.inpFL.append("1, " + str(j + 1) + ", 1 \n")
        #
        # Add *End Part
        self.inpFL.append("*End Part \n")

    def fixGMSHpartForFluent(self, w):
        """
        Used to creat an assembly INP file that is readable by Fluent's mesh importer.
        The assembly INP file that is created using fixGMSHpart is NOT compatible with Fluent's importer.
        Saves the modeler from having to generate meshes manually.
        """
        # Populate self.nodes and self.elements arrays.
        self.fixGMSHpart()
        nNodes = self.__modifyNodeNumbers(w)
        self.inpFL = []
        #
        # *Nodes block
        self.inpFL.append("*Node \n")
        for node in self.nodes:
            nodeCoordStr = ", ".join(['%.10e' % coord for coord in node[1:]])
            self.inpFL.append(str(int(node[0])) + ", " + nodeCoordStr + " \n")
        #
        # *Elements block
        self.inpFL.append("*Element, type=" + self.elmType + ', ELSET=Volume' + str(self.matID) + " \n")
        for ele in self.elements:
            nodeCoordStr = ", ".join([str(int(coord)) for coord in ele[0:]])
            self.inpFL.append(nodeCoordStr + " \n")
        #
        # *ELSET block
        self.inpFL.append("*ELSET, ELSET=" + str(self.matID) + " \n")
        for ele in self.elements:
            self.inpFL.append(str(int(ele[0])) + ", \n")
        return nNodes

    def __modifyNodeNumbers(self, w):
        nNodes = self.nodes.shape[0]
        nEles = self.elements.shape[0]
        self.nodes[:, 0] += w
        self.elements[:, :] += w
        self.elements[:, 0] += nNodes
        return nNodes + w + nEles


class GMSHcube(GMSHpart):
    """
    Specific attributes for a cube geometry.
    use ex:
        GMSHcube([0, 0, 0], [1, 1, 1], [10, 10, 10],
            matID=01, cellID=01)
    """
    def __init__(self, origin, dims, partitions, **kwargs):
        super(GMSHcube, self).__init__(**kwargs)
        self.templateFile = os.path.join(dir, "meshes/block_template.geo")
        self.origin = np.array(origin)
        self.dims = np.array(dims)
        self.partitions = np.array(partitions)
        # used to determine average temperatue in cell
        # self.integrationLimits = np.array([self.origin,
        #                                    self.origin + self.dims,
        #                                    self.origin + np.array([dims[0], 0, 0]),
        #                                    self.origin + np.array([0, dims[1], 0]),
        #                                    self.origin + np.array([0, 0, dims[2]]),
        #                                    self.origin + np.array([dims[0], dims[1], 0]),
        #                                    self.origin + np.array([dims[0], 0, dims[2]]),
        #                                    self.origin + np.array([0, dims[1], dims[2]])])

    def injectParameters(self):
        """
        Run Parser to produce completed GMSH geo file
        """
        paramDict = {'x0': self.origin[0],
                     'y0': self.origin[1],
                     'z0': self.origin[2],
                     'dx': self.dims[0],
                     'dy': self.dims[1],
                     'dz': self.dims[2],
                     'px': self.partitions[0],
                     'py': self.partitions[1],
                     'pz': self.partitions[2],
                     'matID': self.matID}
        injector(self.templateFile, self.geoFile, paramDict)


class GMSHwedge(GMSHpart):
    """
    Specific attributes for a wedge geometry.
    """
    def __init__(self, origin, dims, partitions, **kwargs):
        super(GMSHwedge, self).__init__(**kwargs)
        self.templateFile = os.path.join(dir, "meshes/wedge_template.geo")
        self.origin = np.array(origin)
        self.dims = np.array(dims)
        self.partitions = np.array(partitions)
        # self.integrationLimits

    def injectParameters(self):
        """
        Run Parser to produce completed GMSH geo file
        """
        paramDict = {'x0': self.origin[0],
                     'y0': self.origin[1],
                     'z0': self.origin[2],
                     't0': self.origin[3],
                     'dr': self.dims[0],
                     'dt': self.dims[1],
                     'dz': self.dims[2],
                     'pr': self.partitions[0],
                     'pa': self.partitions[1],
                     'pz': self.partitions[2],
                     'matID': self.matID}
        injector(self.templateFile, self.geoFile, paramDict)


class GMSHring(GMSHpart):
    """
    Specific attributes for a ring geometry.
    """
    def __init__(self, origin, dims, partitions, **kwargs):
        super(GMSHring, self).__init__(**kwargs)
        self.templateFile = os.path.join(dir, "meshes/ring_template.geo")
        self.origin = np.array(origin)
        self.dims = np.array(dims)
        self.partitions = np.array(partitions)
        # self.integrationLimits

    def injectParameters(self):
        """
        Run Parser to produce completed GMSH geo file
        """
        paramDict = {'x0': self.origin[0],
                     'y0': self.origin[1],
                     'z0': self.origin[2],
                     'r0': self.origin[3],
                     't0': self.origin[4],
                     'r1': self.dims[0],
                     'dt': self.dims[1],
                     'dz': self.dims[2],
                     'pr': self.partitions[0],
                     'pa': self.partitions[1],
                     'pz': self.partitions[2],
                     'matID': self.matID}
        injector(self.templateFile, self.geoFile, paramDict)


class GMSHoctant(GMSHpart):
    """
    Specific attributes for a fluid octant geometry.
    """
    def __init__(self, origin, dims, partitions, **kwargs):
        super(GMSHoctant, self).__init__(**kwargs)
        self.templateFile = os.path.join(dir, "meshes/octant_template.geo")
        self.origin = origin
        self.dims = dims
        self.partitions = np.array(partitions)
        # self.integrationLimits

    def injectParameters(self):
        """
        Run Parser to produce completed GMSH geo file
        """
        paramDict = {'x0': self.origin[0],
                     'y0': self.origin[1],
                     'z0': self.origin[2],
                     'r0': self.origin[3],
                     't0': self.origin[4],
                     'pp': self.dims[0],
                     'dt': self.dims[1],
                     'dz': self.dims[2],
                     'pr': self.partitions[0],
                     'pa': self.partitions[1],
                     'pz': self.partitions[2],
                     'matID': self.matID}
        injector(self.templateFile, self.geoFile, paramDict)


class GMSHstl(GMSHpart):
    """
    Generic .stl import part
    """
    def __init__(self, stlfile, trans, size, rotate, **kwargs):
        super(GMSHstl, self).__init__(**kwargs)
        self.templateFile = os.path.join(dir, "meshes/stl_template.geo")
        self.stlfile = stlfile  # str full path to stl file
        # translate part
        self.trans = trans  # len 3 array (dx, dy, dz)
        # dialate part
        self.size = size    # len 4 array (xmult, ymult, zmult, allmult)
        # rotate part
        self.rotate = np.array(rotate)  # len 7 array

    def injectParameters(self):
        paramDict = {'stlFile': self.stlfile,
                     'dx': self.trans[0],
                     'dy': self.trans[1],
                     'dz': self.trans[2],
                     'ix': self.size[0],
                     'iy': self.size[1],
                     'iz': self.size[2],
                     'sc': self.size[3],
                     'ax': self.rotate[0],
                     'ay': self.rotate[1],
                     'az': self.rotate[2],
                     'p1': self.rotate[3],
                     'p2': self.rotate[4],
                     'p3': self.rotate[5],
                     'rt': self.rotate[6],
                     'matID': self.matID}
        injector(self.templateFile, self.geoFile, paramDict)


class GMSHassembly(object):
    """
    Constructs a MCNP compatible assembly .inp file to be used with MCNP6
    unstructured mesh card: EMBED.
    EMBED card requires abaqus formated inp file to function.
    """
    def __init__(self, GMSHparts, inpFileName, asmName='assembly'):
        self.parts = GMSHparts
        self.asmName = asmName
        self.inpFileName = os.path.join(inpFileName, self.asmName + '.inp')

    def __checkForOverlap(self):
        """
        Checks for overlapping cells.  If found throw bad mesh error.
        """
        pass

    def collectParts(self):
        pass

    def buildAssembly(self):
        """
        Gather all inp formatted parts and adjoin them.
        """
        self.partsAsmINP = []
        for part in self.parts:
            self.partsAsmINP += part.getINP()
        self.__createAssemblySection()

    def __createAssemblySection(self):
        """
        Using materialID's and cellID's construct a MCNP compatible
        assembly section at the end of the .inp file
        """
        self.asmSecINP, self.matSecINP, self.densSecINP = [], [], []
        self.asmSecINP.append('** Assembly Section \n')
        self.asmSecINP.append('*Assembly, name=' + self.asmName + ' \n')
        self.asmSecINP.append('** Part Instances \n')
        for i, part in enumerate(self.parts):
            self.asmSecINP.append('*Instance, name=Part-' + str(part.cellID) + "-" + str(i) + ', part=Part-'
                                  + str(part.cellID) + ' \n')
            self.asmSecINP.append('*End Instance \n')
            self.matSecINP.append('*Material, name=material_' + str(part.matID) + ' \n')
            self.matSecINP.append('*Density \n')
            self.matSecINP.append(str(part.density) + ', \n')
        self.asmSecINP.append('*End Assembly \n')

    def writeAsmINP(self, **kwargs):
        completeInpFileContents = self.partsAsmINP + self.asmSecINP + self.matSecINP + self.densSecINP
        #meshBaseDir = kwargs.pop('meshBaseDir', os.path.join(dir, 'meshes/'))
        #self.inpFileName = os.path.join(meshBaseDir, self.asmName + '.inp')
        listToFile(self.inpFileName, completeInpFileContents)


########################
#   HELPER FUNCTIONS   #
########################


def fileToList(infile):
    '''
    list generator helper function so we only need to read the file
    in once and store its contents in some variable.  No need to
    open and close a file a bunch.

    :param infile:  File name
    :type infile: string
    :returns:  lines (list): list of strings that comprise the file
    '''
    try:
        f = open(infile, 'r')
        lines = [line for line in f]
        f.close()
        return lines
    except:
        return None


def listToFile(fileName, fileLines):
    createPath(fileName)
    f1 = open(fileName, 'w')
    for line in fileLines:
        f1.write(line)
    f1.close()


def createPath(fileName):
    filePath = os.path.dirname(fileName)
    if not os.path.exists(filePath):
        os.makedirs(filePath)


def checkLine(line, reFlags):
    """
    Checks lines for flagged strings.  Checks each line
    against regular expession dictionary.
    """
    for flagName, reF in reFlags.iteritems():
        match = reF.match(line)
        if match:
            return (flagName, match)
        else:
            pass
    return None


def injector(infile, outfile, paramDict):
    """
    Builds the completed gmsh input script.
    Takes template script, outputs completed gmsh .geo script.
    """
    f1 = open(infile, 'r')
    createPath(outfile)
    f2 = open(outfile, 'w')
    for line in f1:
        matches = re.findall(r"<(\w+)>", line)
        if not matches:
            # Do nothing if no match found on line
            f2.write(line)
        else:
            # Else modify line
            newline = line
            for match in matches:
                try:
                    substitute = str(paramDict[match])
                    newline = newline.replace('<' + match + '>', substitute)
                except:
                    print("FATAL: Param " + match + " not found in param dictionary")
                    sys.exit("Check template .geo file")
            f2.write(newline)
    f1.close()
    f2.close()


if __name__ == "__main__":
    """
    For testing only.
    """
    myCube = GMSHcube([0, 0, 0], [1, 1, 1], [10, 10, 10])
    myCube.injectParameters()
    myCube.runPartGMSH()
    myCube.writePartINP()
    myBlock = GMSHcube([0, 0, 1], [1, 1, 1], [10, 10, 10], cellID=2, matID=2, source=True)
    myBlock.injectParameters()
    myBlock.runPartGMSH()
    myBlock.writePartINP()
    #
    myAsm = GMSHassembly([myCube, myBlock])
    myAsm.buildAssembly()
    myAsm.writeAsmINP()

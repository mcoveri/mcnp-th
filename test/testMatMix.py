#!/usr/bin/python


import unittest
import numpy as np
import materialMixer as mm
import gmshDriver as gmsh
import mcnpDriver as mcnp


class TestMatMixer(unittest.TestCase):

    def testCheckConsistency(self):
        """
        Checks that a material created using weight fractions
        yeilds same density as material generated using atomic
        fractions
        """
        leuMat = mm.mixedMat({'U235': 4.88801e-4,
                              'U238': 2.23844e-2,
                              'U234': 4.04814e-6,
                              'U236': 2.23756e-6,
                              'O16':  4.57591e-2})
        #print leuMat.density
        self.assertAlmostEqual(leuMat.density, 10.257, 4)
        fuelMat = mm.mixedMat({'U235': 0.005, 'U238': 0.995})
        fuelMat.setDensity(10.2)  # g/cc
        lwtrMat = mm.mixedMat({'O16': 1.0 / 3.0, 'H1': 2.0 / 3.0})
        lwtrMat.setDensity(1.0)  # g/cc
        coreMat = lwtrMat * 0.7 + fuelMat * 0.3
        # use core mat weight fractions to seed a new material and check for
        # consitancy
        checkMat = mm.mixedMat(coreMat.wf, wfFlag=True)
        checkMat.setDensity(coreMat.density)
        #print coreMat.nDdict
        #print checkMat.nDdict
        self.assertEqual(coreMat.density, checkMat.density)

    def testDensityChange(self):
        """
        Atomic ratios must remain consistant after density change.
        """
        leuMat = mm.mixedMat({'U235': 4.88801e-4,
                              'U238': 2.23844e-2,
                              'U234': 4.04814e-6,
                              'U236': 2.23756e-6,
                              'O16':  4.57591e-2})
        afRatio1 = leuMat.nDdict['O16'] / sum(leuMat.nDdict.values())
        leuMat.setDensity(5.2)
        afRatio2 = leuMat.nDdict['O16'] / sum(leuMat.nDdict.values())
        self.assertAlmostEqual(afRatio1, afRatio2, 10)
        # Also check wf
        leuMat.setDensity(10.257)
        wfRatio1 = leuMat.wf['O16'] / sum(leuMat.wf.values())
        leuMat.setDensity(5.2)
        wfRatio2 = leuMat.wf['O16'] / sum(leuMat.wf.values())
        self.assertAlmostEqual(wfRatio1, wfRatio2, 10)

    def testFuelMix(self):
        """
        Test ability to make different fuel enrichments
        """
        pass

    def testZirMix(self):
        """
        Test ability to mix zirconium at different temperatures
        """
        zircMat = mm.mixedMat({'Cr50':  3.30121E-06,
                               'Cr52':  6.36606E-05,
                               'Cr53':  7.21860E-06,
                               'Cr54':  1.79686E-06,
                               'Fe54':  8.68307E-06,
                               'Fe56':  1.36306E-04,
                               'Fe57':  3.14789E-06,
                               'Fe58':  4.18926E-07,
                               'Zr90':  2.18865E-02,
                               'Zr91':  4.77292E-03,
                               'Zr92':  7.29551E-03,
                               'Zr94':  7.39335E-03,
                               'Zr96':  1.19110E-03,
                               'Sn112': 4.68066E-06,
                               'Sn114': 3.18478E-06,
                               'Sn115': 1.64064E-06,
                               'Sn116': 7.01616E-05,
                               'Sn117': 3.70592E-05,
                               'Sn118': 1.16872E-04,
                               'Sn119': 4.14504E-05,
                               'Sn120': 1.57212E-04,
                               'Sn122': 2.23417E-05,
                               'Sn124': 2.79392E-05,
                               'Hf174': 3.54138E-09,
                               'Hf176': 1.16423E-07,
                               'Hf177': 4.11686E-07,
                               'Hf178': 6.03806E-07,
                               'Hf179': 3.01460E-07,
                               'Hf180': 7.76449E-07})
        zircMat.setDensity(6.56)
        #print zircMat.nDdict
        # Create zircMat with GMSHpart interface
        tempWedge = gmsh.GMSHwedge([0, 0, 0, 0], [0.408 / 2., np.pi / 2, 1.0], [3, 9, 3],
                                   source=False, cellID=1, matID=1, matName={'02-zirc': 1.0},
                                   density=6.56, temperature=900.)
        tempWedge.injectParameters()
        tempWedge.runPartGMSH()
        tempAsm = gmsh.GMSHassembly([tempWedge], 'asm')
        tempMCNP = mcnp.MCNPdriver(tempAsm)
        matCards, gmshHomogenizednDdict = tempMCNP.materialMix(tempMCNP.gmshAsm.parts[0])
        #print gmshHomogenizednDdict
        self.assertAlmostEqual(gmshHomogenizednDdict['40094.72c'], zircMat.nDdict['Zr94'], 6)
        self.assertAlmostEqual(gmshHomogenizednDdict['40092.72c'], zircMat.nDdict['Zr92'], 6)
        self.assertAlmostEqual(gmshHomogenizednDdict['40096.72c'], zircMat.nDdict['Zr96'], 6)
        self.assertAlmostEqual(sum(zircMat.nDdict.values()), sum(gmshHomogenizednDdict.values()), 4)

    def testMultiMatMix(self):
        """
        Test ability to make a homognized mixture of moderator
        and water.
        """
        leuMat = mm.mixedMat({'U235': 4.88801e-4,
                              'U238': 2.23844e-2,
                              'U234': 4.04814e-6,
                              'U236': 2.23756e-6,
                              'O16':  4.57591e-2})
        leuMat.setDensity(10.24)
        lwtrMat = mm.mixedMat({'H1': 2. / 3.,
                               'O16': 1. / 3.})
        lwtrMat.setDensity(1.0)
        homogenizedMat = leuMat * 0.7 + lwtrMat * 0.3
        #print homogenizedMat.density
        #print homogenizedMat.nDdict
        #
        # Create homogenized mat through GMSHpart interface
        tempWedge = gmsh.GMSHwedge([0, 0, 0, 0], [0.408 / 2., np.pi / 2, 1.0], [3, 9, 3],
                                   source=True, cellID=1, matID=1, matName={'21-fuel': (0.7, 10.24), '00-lwtr': (0.3, 1.0)},
                                   density=homogenizedMat.density, temperature=900.)
        tempWedge.injectParameters()
        tempWedge.runPartGMSH()
        tempAsm = gmsh.GMSHassembly([tempWedge], 'asm')
        tempMCNP = mcnp.MCNPdriver(tempAsm)
        matCards, gmshHomogenizednDdict = tempMCNP.materialMix(tempMCNP.gmshAsm.parts[0])
        #print gmshHomogenizednDdict
        self.assertAlmostEqual(gmshHomogenizednDdict['92238.72c'], homogenizedMat.nDdict['U238'], 6)
        self.assertAlmostEqual(gmshHomogenizednDdict['92236.72c'], homogenizedMat.nDdict['U236'], 6)
        self.assertAlmostEqual(gmshHomogenizednDdict['92235.72c'], homogenizedMat.nDdict['U235'], 6)
        self.assertAlmostEqual(gmshHomogenizednDdict['92234.72c'], homogenizedMat.nDdict['U234'], 6)
        self.assertAlmostEqual(gmshHomogenizednDdict['8016.72c'], homogenizedMat.nDdict['O16'], 6)
        self.assertAlmostEqual(gmshHomogenizednDdict['1001.72c'], homogenizedMat.nDdict['H1'], 6)

    def testTemperatureMix(self):
        """
        Tests ability to weight isotopes by
        temperature distance to bounding nodes.
        """
        tempWedge = gmsh.GMSHwedge([0, 0, 0, 0], [0.408 / 2., np.pi / 2, 1.0], [3, 9, 3],
                                   source=True, cellID=1, matID=1, matName={'21-fuel': (0.7, 10.24), '00-lwtr': (0.3, 1.0)},
                                   density=0.7, temperature=525.)
        tempWedge.injectParameters()
        tempWedge.runPartGMSH()
        tempAsm = gmsh.GMSHassembly([tempWedge], 'asm')
        tempMCNP = mcnp.MCNPdriver(tempAsm)
        matCards, gmshNDdict1 = tempMCNP.materialMix(tempMCNP.gmshAsm.parts[0])
        tempWedge = gmsh.GMSHwedge([0, 0, 0, 0], [0.408 / 2., np.pi / 2, 1.0], [3, 9, 3],
                                   source=True, cellID=1, matID=1, matName={'21-fuel': (0.7, 10.24), '00-lwtr': (0.3, 1.0)},
                                   density=0.7, temperature=320.)
        tempWedge.injectParameters()
        tempWedge.runPartGMSH()
        tempAsm = gmsh.GMSHassembly([tempWedge], 'asm')
        tempMCNP = mcnp.MCNPdriver(tempAsm)
        matCards, gmshNDdict2 = tempMCNP.materialMix(tempMCNP.gmshAsm.parts[0])
        self.assertAlmostEqual(sum(gmshNDdict1.values()), sum(gmshNDdict2.values()), 8)

    def testHighT(self):
        """
        High temperature mixture.  Temperature greater than highest avalible xsection
        """
        tempWedge = gmsh.GMSHwedge([0, 0, 0, 0], [0.408 / 2., np.pi / 2, 1.0], [3, 9, 3],
                                   source=True, cellID=1, matID=1, matName={'31-fuel': (0.7, 10.24), '00-lwtr': (0.3, 1.0)},
                                   density=0.7, temperature=2500.)
        tempWedge.injectParameters()
        tempWedge.runPartGMSH()
        tempAsm = gmsh.GMSHassembly([tempWedge], 'asm')
        tempMCNP = mcnp.MCNPdriver(tempAsm)
        matCards, gmshNDdict1 = tempMCNP.materialMix(tempMCNP.gmshAsm.parts[0])

    def testLowT(self):
        """
        Low temperature mixture.  Temperature lower than lowest avalible xsection
        """
        tempWedge = gmsh.GMSHwedge([0, 0, 0, 0], [0.408 / 2., np.pi / 2, 1.0], [3, 9, 3],
                                   source=True, cellID=1, matID=1, matName={'31-fuel': (0.7, 10.24), '00-lwtr': (0.3, 1.0)},
                                   density=0.7, temperature=100.)
        tempWedge.injectParameters()
        tempWedge.runPartGMSH()
        tempAsm = gmsh.GMSHassembly([tempWedge], 'asm')
        tempMCNP = mcnp.MCNPdriver(tempAsm)
        matCards, gmshNDdict1 = tempMCNP.materialMix(tempMCNP.gmshAsm.parts[0])

    def testMedT(self):
        """
        Low temperature mixture.  Temperature lower than lowest avalible xsection
        """
        tempWedge = gmsh.GMSHwedge([0, 0, 0, 0], [0.408 / 2., np.pi / 2, 1.0], [3, 9, 3],
                                   source=True, cellID=1, matID=1, matName={'31-fuel': (0.7, 10.24), '00-lwtr': (0.3, 1.0)},
                                   density=0.7, temperature=1200.)
        tempWedge.injectParameters()
        tempWedge.runPartGMSH()
        tempAsm = gmsh.GMSHassembly([tempWedge], 'asm')
        tempMCNP = mcnp.MCNPdriver(tempAsm)
        matCards, gmshNDdict1 = tempMCNP.materialMix(tempMCNP.gmshAsm.parts[0])


if __name__ == "__main__":
    unittest.main()

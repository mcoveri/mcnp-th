#!/usr/bin/python

# Tests mesh to mesh interpolation routines.
#

import unittest
import numpy as np
import gmshDriver
import meshData

# class TestMeshConstruction(object):
#     def testBlockMesh(self):
#         """
#         Checks to ensure a plane wall geometry can be
#         constructed successfully.
#         """
#         pass
#
#     def testPinMesh(self):
#         """
#         Checks that a pincell mesh is constructed successfully
#         """
#         pass


class TestMesh2Mesh(unittest.TestCase):
    """
    Tests volume integral capabilites of the
    dataMesh class.
    """
    def testVolumeCalcCyl(self):
        """
        Ensures accuracy volume estimate for a cyl
        """
        # Compute true volume
        r, h = 0.5, 2.0
        trueVol = h * np.pi * r ** 2
        # Create a cyl mesh
        cyl = gmshDriver.GMSHwedge([0, 0, 0, 0], [r, np.pi, h], [4, 18, 10],
                                   source=False, cellID=1, matName={'21-fuel': 1.0},
                                   desnity=-10.24)
        cyl.injectParameters()
        cyl.runPartGMSH()

        def cosFn(x):
            return np.cos(x[:, 0]) + np.cos(x[:, 1])
        myMesh = meshData.MeshData(None, 'function', interpFn=cosFn)
        aproxVol, cellAvg = myMesh.setCellProp(cyl)
        print("=========== Cyl Vol Test =============")
        print('Truth: %f , Est: %f , diff: %f' % (trueVol, aproxVol * 2, (trueVol - aproxVol * 2) / trueVol))
        self.assertAlmostEqual(aproxVol * 2., trueVol, 2)

    def testWedgeVolume(self):
        r1, r2, h = 0.25, 0.5, 2.0
        trueVol = h * np.pi * r2 ** 2 - h * np.pi * r1 ** 2
        cyl = gmshDriver.GMSHring([0, 0, 0, r1, 0], [r2, np.pi, h], [4, 18, 10],
                                  source=False, cellID=1, matName={'21-fuel': 1.0},
                                  desnity=-10.24)
        cyl.injectParameters()
        cyl.runPartGMSH()

        def cosFn(x):
            return np.cos(x[:, 0]) + np.cos(x[:, 1])
        myMesh = meshData.MeshData(None, 'function', interpFn=cosFn)
        aproxVol, cellAvg = myMesh.setCellProp(cyl)
        print("=========== Wedge Vol Test =============")
        print('Truth: %f , Est: %f , diff: %f' % (trueVol, aproxVol * 2, (trueVol - aproxVol * 2) / trueVol))
        self.assertAlmostEqual(aproxVol * 2., trueVol, 2)

    def testUltaHighAspectRatio(self):
        r1, r2, h = 0.25, 0.251, 10.0
        trueVol = h * np.pi * r2 ** 2 - h * np.pi * r1 ** 2
        cyl = gmshDriver.GMSHring([0, 0, 0, r1, 0], [r2, np.pi, h], [2, 18, 4],
                                  source=False, cellID=1, matName={'21-fuel': 1.0},
                                  desnity=-10.24)
        cyl.injectParameters()
        cyl.runPartGMSH()

        def cosFn(x):
            return np.cos(x[:, 0]) + np.cos(x[:, 1])
        myMesh = meshData.MeshData(None, 'function', interpFn=cosFn)
        aproxVol, cellAvg = myMesh.setCellProp(cyl)
        print("=========== High Aspect Ratio Vol Test =============")
        print('Truth: %f , Est: %f , diff: %f' % (trueVol, aproxVol * 2, (trueVol - aproxVol * 2) / trueVol))
        self.assertAlmostEqual(aproxVol * 2., trueVol, 2)

    def testStepFunction(self):
        """
        Compares volume integral of step function to
        analytic solution for accuracy using several
        grid sizes.
        """
        #assertAlmostEqual(a, b, 5)
        pass

    def testCosineFunction(self):
        """
        Compares volume integral of cosine function to
        analytic solution for accuracy using several
        grid sizes.
        """
        cube = gmshDriver.GMSHcube([-1, -1, 0], [1, 1, 1], [40, 40, 10],
                                   source=False, cellID=1, matName={'21-fuel': 1.0},
                                   density=-10.24)
        cube.injectParameters()
        cube.runPartGMSH()

        def cosFn(x):
            return np.cos(x[:, 0]) + np.cos(x[:, 1])
        trueVal = 8. * np.sin(1.0) / 4.
        myMesh = meshData.MeshData(None, 'function', interpFn=cosFn)
        aproxVol, cellAvg = myMesh.setCellProp(cube)
        print("=========== Cosine Integral Test =============")
        print('Truth: %f , Est: %f , diff: %f' % (trueVal, cellAvg, (trueVal - cellAvg) / trueVal))
        self.assertAlmostEqual(cellAvg, trueVal, 4)

    def testQuadraticFunction(self):
        cube = gmshDriver.GMSHcube([-1, -1, 0], [1, 1, 1], [15, 20, 5],
                                   source=False, cellID=1, matName={'21-fuel': 1.0},
                                   density=-10.24)
        cube.injectParameters()
        cube.runPartGMSH()

        def quadFn(x):
            return x[:, 0] ** 2 + x[:, 1] ** 2
        trueVal = (8. / 3.) / 4.
        myMesh = meshData.MeshData(None, 'function', interpFn=quadFn)
        aproxVol, cellAvg = myMesh.setCellProp(cube)
        print("=========== Quadratic Integral Test =============")
        print('Truth: %f , Est: %f , diff: %f' % (trueVal, cellAvg, (trueVal - cellAvg) / trueVal))
        self.assertAlmostEqual(cellAvg, trueVal, 3)

    def testSinFunction(self):
        """
        Compares volume integral of a wonky sin function to
        analytic solution for accuracy using several
        grid sizes.
        """
        # Create a cube with side lengths = 2
        cube = gmshDriver.GMSHcube([-1, -1, -1], [1, 1, 1], [15, 15, 15],
                                   source=False, cellID=1, matName={'21-fuel': 1.0},
                                   density=-10.24)
        cube.injectParameters()
        cube.runPartGMSH()

        def crazySin(x):
            return np.sin(x[:, 0] * x[:, 1] * x[:, 2]) / (x[:, 0] * x[:, 1] * x[:, 2])
        trueVal = 7.95115 / 8.
        myMesh = meshData.MeshData(None, 'function', interpFn=crazySin)
        aproxVol, cellAvg = myMesh.setCellProp(cube)
        print("=========== Sin Integral Test 1.0 =============")
        print('Truth: %f , Est: %f , diff: %f' % (trueVal, cellAvg, (trueVal - cellAvg) / trueVal))
        self.assertAlmostEqual(cellAvg, trueVal, 4)

    def testSinFunction2(self):
        """
        Compares volume integral of a wonky sin function to
        analytic solution for accuracy using several
        grid sizes.
        """
        # Create a cube with side lengths = 2
        cube = gmshDriver.GMSHcube([-1, -1, -1], [1, 1, 1], [15, 15, 15],
                                   source=False, cellID=1, matName={'21-fuel': 1.0},
                                   density=-10.24)
        cube.injectParameters()
        cube.runPartGMSH()

        def crazySin(x):
            return np.sin(x[:, 0] * x[:, 1] * x[:, 2] * 10.) / (x[:, 0] * x[:, 1] * x[:, 2] * 10.)
        trueVal = 5.72688 / 8.
        myMesh = meshData.MeshData(None, 'function', interpFn=crazySin)
        aproxVol, cellAvg = myMesh.setCellProp(cube)
        print("=========== Sin Integral Test 2.0 =============")
        print('Truth: %f , Est: %f , diff: %f' % (trueVal, cellAvg, (trueVal - cellAvg) / trueVal))
        self.assertAlmostEqual(cellAvg, trueVal, 4)

if __name__ == "__main__":
    unittest.main()

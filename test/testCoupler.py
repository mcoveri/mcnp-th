#!/usr/bin/python


import unittest
import numpy as np
from gmshDriver import *
import meshData
import os
from coupler import CoupledMCFL
mydir = os.getcwd()
baseDir = mydir + '/test/cases/unitcell'


def dummyGeomSetup(**kwargs):
    """ Setup quarter sym pin cell case geometry. """
    meshBaseDir = os.path.join(baseDir, "mesh/")
    fuelBlocks, zircBlocks, modBlocks = [], [], []
    dz = 10.0
    pz = 3
    zmax = 5.0
    ppitch = 1.260
    nPhi = 12  # azimuthal segments
    radCorrs = {4: 0.005708, 6: 0.002056, 9: 0.000803, 12: 0.0}  # radial correction factors
    radCorr = radCorrs.get(nPhi)
    fuelrmax = 0.4096
    hermax = 0.418
    zircrmax = 0.475
    # Correct radii due to discritization error
    fuelrmax += fuelrmax * radCorr
    hermax += hermax * radCorr
    zircrmax += zircrmax * radCorr
    z0s = np.arange(0, zmax, dz)
    x0s = np.array([0])
    y0s = np.array([0])
    quadrents = ['0', 'Pi / 2', 'Pi', 'Pi * (3 / 2)']
    i = 1
    for x0 in x0s:
        for y0 in y0s:
            for z0 in z0s:
                for quad in quadrents:
                    # fuel
                    tempWedge = GMSHwedge([x0, y0, z0, quad], [fuelrmax / 2., np.pi / 2, dz], [3, nPhi, pz],
                                          source=True, cellID=10000 + i, matID=10000 + i, matName={'21-fuel': 1.0},
                                          density=-10.24, temperature=900.,
                                          geoFile=os.path.join(meshBaseDir, "Part-" + str(10000 + i) + ".geo")
                                          )
                    tempWedge.injectParameters()
                    tempWedge.runPartGMSH()
                    fuelBlocks.append(tempWedge)
                    i += 1
                    # fuel 2
                    tempRing = GMSHring([x0, y0, z0, fuelrmax / 2., quad], [fuelrmax, np.pi / 2, dz], [3, nPhi, pz],
                                        source=True, cellID=10000 + i, matID=10000 + i, matName={'21-fuel': 1.0},
                                        density=-10.24, temperature=900.,
                                        geoFile=os.path.join(meshBaseDir, "Part-" + str(10000 + i) + ".geo")
                                        )
                    tempRing.injectParameters()
                    tempRing.runPartGMSH()
                    fuelBlocks.append(tempRing)
                    i += 1
                    # cladding
                    tempRing = GMSHring([x0, y0, z0, hermax, quad], [zircrmax, np.pi / 2, dz], [3, nPhi, 3],
                                        source=False, cellID=i, matID=i, matName={'02-zirc': 1.0},
                                        density=-6.56, temperature=600.,
                                        geoFile=os.path.join(meshBaseDir, "Part-" + str(i) + ".geo")
                                        )
                    tempRing.injectParameters()
                    tempRing.runPartGMSH()
                    zircBlocks.append(tempRing)
                    i += 1
                    # moderator octant sym blocks
                    tempOct = GMSHoctant([x0, y0, z0, zircrmax, quad], [ppitch, 'Pi / 4.', dz], [3, nPhi, 3],
                                         source=False, cellID=i, matID=i, matName={'00-lwtr': 1.0},
                                         density=-0.8, temperature=600.,
                                         geoFile=os.path.join(meshBaseDir, "Part-" + str(i) + ".geo")
                                         )
                    tempOct.injectParameters()
                    tempOct.runPartGMSH()
                    modBlocks.append(tempOct)
                    i += 1
                    tempOct = GMSHoctant([x0, y0, z0, zircrmax, quad + ' + Pi / 4.'], [ppitch, 'Pi / 4.', dz], [3, nPhi, 3],
                                         source=False, cellID=i, matID=i, matName={'00-lwtr': 1.0},
                                         density=-0.8, temperature=600.,
                                         geoFile=os.path.join(meshBaseDir, "Part-" + str(i) + ".geo")
                                         )
                    tempOct.injectParameters()
                    tempOct.runPartGMSH()
                    modBlocks.append(tempOct)
                    i += 1
    myAsm = GMSHassembly(fuelBlocks + zircBlocks + modBlocks, meshBaseDir, 'asm')
    myAsm.buildAssembly()
    myAsm.writeAsmINP(meshBaseDir=meshBaseDir)
    return myAsm


class DummyDeck(object):
    def __init__(self):
        ###########################
        ### MCNP PARAMETER DICT ###
        ###########################
        self.MCNPparams = {'runName': 'unitCell',
                    'mcnpBaseDir': os.path.join(baseDir, 'mcnp/'),
                    'eeoutFile': os.path.join(baseDir, 'mesh/asm.eeout'),
                    'bc_axl': 'ref',
                    'cores': 23,
                    'kcode': {0: '5e4 1.0 65 220'}
                    }

        #############################
        ### FLUENT PARAMETER DICT ###
        #############################
        self.fluentParams = {'fluentCasName': os.path.join(baseDir, 'fluent/cases/pinCell/pinCell.cas'),
                        'templateJournal': os.path.join(baseDir, 'fluent/journals/template_journal.jou'),
                        'THDim': 3,
                        'THheatSource': {'solid_source_ip':
                                        [os.path.join(baseDir, 'fluent/cases/pinCell/pinCell_source_solid.ip'),
                                        (10000, 20000),
                                        'uds-0']
                                        },
                        'THdensityData': os.path.join(baseDir, 'fluent/cases/pinCell/pinCell_density.out'),
                        'THtempData': os.path.join(baseDir, 'fluent/cases/pinCell/pinCell_temperature.out'),
                        'THiters': 600
                        }

        ############################
        ### MESH TO MESH SETINGS ###
        ############################
        self.meshParams = {'mcnp2TH_tr': [0., 0., 0.],   # shift MC f6 tally result to TH ordinates [cm]
                    'mcnp2TH_reshape': [0, 1, 2],          # swap z coords with y coords
                    'mcnp2TH_scale': 0.01,                 # convert cm to m
                    'TH2mcnp_tr': [0., 0., 0.],         # shift TH result to MCNP grid ordinates [cm]
                    'TH2mcnp_reshape': [0, 1, 2],          # swaps y coords with z coords
                    'TH2mcnp_scale': 100                   # convert meters to cm
                    }

        ############################
        ###    COUPLER SETINGS   ###
        ############################
        self.couplerParams = {'outerIters': 0,
                        'coupled': False,
                        'qTol': 1e-2,      # vol heat gen field convergence tolorance
                        'tTol': 1e-2,      # temperature field convergence tolorance
                        'kTol': 50e-5,
                        'iRlx': 0,         # outer iteration at which to begin back-averaging heating tally
                        'maxBI': 2,
                        'minWI': 0.5,
                        'powerNorm':  4. * 6.93e7,  # Power normalization factor (asm avg vol heat source W/m^3)
                        'logDir': os.path.join(baseDir, 'logs/corVol/unitCell_20p'),
                        'updateMask': [9998, 9999]  # cellIDs to ignore density and temperature updates
                        }
        pass


class TestCoupler(unittest.TestCase):
    def testBackAverageQ_3back(self):
        """
        Tests ability to average 3 past iterations into the leading solution vector.
        """
        dummyDeck = DummyDeck()
        # Create a dummy coupled run.
        coupledRun = CoupledMCFL(dummyGeomSetup, dummyDeck)
        coupledRun.qResult = []
        # fabricate dummy f6Tally results
        dummyF6tally1 = np.ones((4, 10)) * 1
        dummyF6tally2 = np.ones((4, 10)) * 2
        dummyF6tally3 = np.ones((4, 10)) * 3
        coupledRun.f6Tally = meshData.MeshData(None, 'function', scalarField=dummyF6tally1)
        coupledRun.qResult.append(coupledRun.f6Tally)
        coupledRun.f6Tally = meshData.MeshData(None, 'function', scalarField=dummyF6tally2)
        coupledRun.qResult.append(coupledRun.f6Tally)
        coupledRun.f6Tally = meshData.MeshData(None, 'function', scalarField=dummyF6tally3)
        coupledRun.i = 2
        # Call backAverageQ
        coupledRun.backAverageQ(0, 3, 1 / 3.)
        #coupledRun.qResult.append(coupledRun.f6Tally)
        # Check for expected behavior
        self.assertAlmostEqual(coupledRun.f6Tally.scalarField[-1][-1], 2.3333333, 5)
        coupledRun.backAverageQ(0, 2, 1 / 3.)  # avg of 0.25 * 2.0 and 0.75 * 2.3333
        coupledRun.qResult.append(coupledRun.f6Tally)
        self.assertAlmostEqual(coupledRun.f6Tally.scalarField[-1][-1], 2.25, 5)
        #print(coupledRun.f6Tally.scalarField)

    def testBackAverageQ_2(self):
        """
        Tests ability to average 2 past iterations into the leading solution vector.
        """
        dummyDeck = DummyDeck()
        # Create a dummy coupled run.
        coupledRun = CoupledMCFL(dummyGeomSetup, dummyDeck)
        coupledRun.qResult = []
        # fabricate dummy f6Tally results
        dummyF6tally1 = np.ones((4, 10)) * 1
        dummyF6tally2 = np.ones((4, 10)) * 2
        coupledRun.f6Tally = meshData.MeshData(None, 'function', scalarField=dummyF6tally1)
        coupledRun.qResult.append(coupledRun.f6Tally)
        coupledRun.f6Tally = meshData.MeshData(None, 'function', scalarField=dummyF6tally2)
        coupledRun.i = 1
        # Call backAverageQ
        coupledRun.backAverageQ(0, 2, 1 / 3.)
        coupledRun.qResult.append(coupledRun.f6Tally)
        # Check for expected behavior
        self.assertAlmostEqual(coupledRun.f6Tally.scalarField[-1][-1], 1.75, 5)
        #print(coupledRun.f6Tally.scalarField)

    def testBackAverageQ_1(self):
        """
        Tests ability to handle 0th iteration when there is not enough solution data
        to conduct under relaxation.
        """
        dummyDeck = DummyDeck()
        # Create a dummy coupled run.
        coupledRun = CoupledMCFL(dummyGeomSetup, dummyDeck)
        coupledRun.qResult = []
        # fabricate dummy f6Tally results
        dummyF6tally1 = np.ones((4, 10)) * 1
        coupledRun.f6Tally = meshData.MeshData(None, 'function', scalarField=dummyF6tally1)
        coupledRun.i = 0
        # Call backAverageQ
        coupledRun.backAverageQ(0, 2, 1 / 3.)
        # Check for expected behavior
        self.assertAlmostEqual(coupledRun.f6Tally.scalarField[-1][-1], 1.0, 5)
        coupledRun.qResult.append(coupledRun.f6Tally)
        #print(coupledRun.f6Tally.scalarField)


if __name__ == "__main__":
    unittest.main()

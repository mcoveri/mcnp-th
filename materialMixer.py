#!/usr/bin/python

#################           MATERIAL CLASS           ################
#
# Mixes materials by weight or atom fraction.
# Originally intended to supply material data to
# a diffusion-theory neutron transport code.
#
# Changelog
# ---------
#
# Author
# ------
# William Gurecky
# william.gurecky@utexas.edu
#
#####################################################################

import numpy as np

# CONSTANTS
# -------------
#

# material properties library
# Store group constants for each energy group.
matLib = {}
# single group cross sections
matLib['U234'] = {'sigma_f': np.array([0.01045]),
                     'sigma_a': np.array([2.4]),
                     'sigma_s': np.array([9.3]),
                     'nu': np.array([2.4]),
                     'M': 234.0409456,  # Atomic mass [kg/mol]
                     'A': 92.0,   # Number of protons
                     'zaid': '92234'}
matLib['U235'] = {'sigma_f': np.array([500.0]),
                     'sigma_a': np.array([500.0 + 86.6]),
                     'sigma_s': np.array([14.9]),
                     'nu': np.array([2.4]),
                     'M': 235.043931,
                     'A': 92.0,
                     'zaid': '92235'}
matLib['U236'] = {'sigma_f': np.array([61.29]),
                     'sigma_a': np.array([5.0]),
                     'sigma_s': np.array([8.]),
                     'nu': np.array([2.4]),
                     'M': 236.0455619,
                     'A': 92.0,
                     'zaid': '92236'}
matLib['U238'] = {'sigma_f': np.array([0.01045]),
                     'sigma_a': np.array([2.4]),
                     'sigma_s': np.array([9.3]),
                     'nu': np.array([2.4]),
                     'M': 238.0507826,
                     'A': 92.0,
                     'zaid': '92238'}
matLib['H1'] = {'sigma_f': np.array([0.0]),
                   'sigma_a': np.array([0.294]),
                   'sigma_s': np.array([20.47]),
                   'M': 1.007825,
                   'A': 1.0,
                   'zaid': '1001'}
matLib['O16'] = {'sigma_f': np.array([0.0]),
                    'sigma_a': np.array([0.2]),
                    'sigma_s': np.array([3.78]),
                    'M': 15.9949146,  # Atomic mass [kg/mol]
                    'A': 8.0,   # Number of protons
                    'zaid': '8016'}
matLib['B10'] = {'sigma_f': np.array([0.0]),
                   'sigma_a': np.array([0.294]),
                   'sigma_s': np.array([20.47]),
                   'M': 10.012937,
                   'A': 5.0,
                   'zaid': '5010'}
matLib['B11'] = {'sigma_f': np.array([0.0]),
                   'sigma_a': np.array([0.294]),
                   'sigma_s': np.array([20.47]),
                   'M': 11.0093055,
                   'A': 5.0,
                   'zaid': '5011'}
matLib['C12'] = {'M': 12.0,
                 'A': 6,
                 'zaid': 6000
                 }
matLib['Si28'] = {'M': 27.9769265,
                  'A': 14,
                  'zaid': 14028
                  }
matLib['Si29'] = {'M': 28.9764947,
                  'A': 14,
                  'zaid': 14029
                  }
matLib['Si30'] = {'M': 29.9737702,
                  'A': 14,
                  'zaid': 14030
                  }
matLib['P31'] = {'M': 30.9737615,
                 'A': 15,
                 'zaid': 15031
                 }
matLib['Cr50'] = {'M': 49.9460496,
                  'A': 24,
                  'zaid': 24050
                  }
matLib['Cr52'] = {'M': 51.9405119,
                  'A': 24,
                  'zaid': 24052
                  }
matLib['Cr53'] = {'M': 52.9406538,
                  'A': 24,
                  'zaid': 24053
                  }
matLib['Cr54'] = {'M': 53.93888,
                  'A': 24,
                  'zaid': 24054
                  }
matLib['Mn55'] = {'M': 54.938049,
                  'A': 25,
                  'zaid': 25055
                  }
matLib['Fe54'] = {'M': 53.93961,
                  'A': 26,
                  'zaid': 26054
                  }
matLib['Fe56'] = {'M': 55.93494,
                  'A': 26,
                  'zaid': 26056
                  }
matLib['Fe57'] = {'M': 56.9353987,
                  'A': 26,
                  'zaid': 26057
                  }
matLib['Fe58'] = {'M': 57.93328,
                  'A': 26,
                  'zaid': 26058
                  }
matLib['Ni58'] = {'M': 57.9353,
                  'A': 28,
                  'zaid': 28058
                  }
matLib['Ni60'] = {'M': 59.9307,
                  'A': 28,
                  'zaid': 28060
                  }
matLib['Ni61'] = {'M': 60.93106,
                  'A': 28,
                  'zaid': 28061
                  }
matLib['Ni62'] = {'M': 61.9283488,
                  'A': 28,
                  'zaid': 28062
                  }
matLib['Ni64'] = {'M': 63.9279696,
                  'A': 28,
                  'zaid': 28064
                  }
matLib['Zr90'] = {'M': 89.904,
                  'A': 40,
                  'zaid': 40090
                  }
matLib['Zr91'] = {'M': 90.9056,
                  'A': 40,
                  'zaid': 40091
                  }
matLib['Zr92'] = {'M': 91.905,
                  'A': 40,
                  'zaid': 40092
                  }
matLib['Zr94'] = {'M': 93.9063168,
                  'A': 40,
                  'zaid': 40094
                  }
matLib['Zr96'] = {'M': 95.90827,
                  'A': 40,
                  'zaid': 40096
                  }
matLib['Sn112'] = {'M': 111.90482,
                   'A': 50,
                   'zaid': 50112
                   }
matLib['Sn114'] = {'M': 113.90278,
                   'A': 50,
                   'zaid': 50114
                   }
matLib['Sn115'] = {'M': 114.90334,
                   'A': 50,
                   'zaid': 50115
                   }
matLib['Sn116'] = {'M': 115.9017441,
                   'A': 50,
                   'zaid': 50116
                   }
matLib['Sn117'] = {'M': 116.9029538,
                   'A': 50,
                   'zaid': 50117
                   }
matLib['Sn118'] = {'M': 117.9016,
                   'A': 50,
                   'zaid': 50118
                   }
matLib['Sn119'] = {'M': 118.9033,
                   'A': 50,
                   'zaid': 50119
                   }
matLib['Sn120'] = {'M': 119.9021966,
                   'A': 50,
                   'zaid': 50120
                   }
matLib['Sn122'] = {'M': 121.90344,
                   'A': 50,
                   'zaid': 50122
                   }
matLib['Sn124'] = {'M': 123.90527,
                   'A': 50,
                   'zaid': 50124
                   }
matLib['Hf174'] = {'M': 173.94,
                   'A': 72,
                   'zaid': 72174
                   }
matLib['Hf176'] = {'M': 175.9414,
                   'A': 72,
                   'zaid': 72176
                   }
matLib['Hf177'] = {'M': 176.94322,
                   'A': 72,
                   'zaid': 72177
                   }
matLib['Hf178'] = {'M': 177.9436977,
                   'A': 72,
                   'zaid': 72178
                   }
matLib['Hf179'] = {'M': 178.9458151,
                   'A': 72,
                   'zaid': 72179
                   }
matLib['Hf180'] = {'M': 179.9465488,
                   'A': 72,
                   'zaid': 72180
                   }

# Stores predefined material compositions, avalible cross section temperatures,
# and XS suffixes for MCNP M cards eg: .70c
mixtureData = {
    # 99 percent enriched fuel
    '99-fuel': {'comp': {'U235': 0.02288,
                         'O16':  4.57642e-2},
                'wfFlag': False,  # is compositin given in weight fracs?
                'density': 10.24,  # g/cc
                'xsT': {293.: '.70c',  # xs info
                        600.: '.71c',
                        900.: '.72c',
                        1200.: '.73c',
                        2500.: '.74c'}
                },
    # 3.1 percent enriched fuel
    '31-fuel': {'comp': {'U235': 7.18132e-4,
                         'U238': 2.21546e-2,
                         'U234': 6.11864e-6,
                         'U236': 3.29861e-6,
                         'O16':  4.57642e-2},
                'wfFlag': False,  # is compositin given in weight fracs?
                'density': 10.24,  # g/cc
                'xsT': {293.: '.70c',  # xs info
                        600.: '.71c',
                        900.: '.72c',
                        1200.: '.73c',
                        2500.: '.74c'}
                },
    # 2.1 percent enriched fuel
    '21-fuel': {'comp': {'U235': 4.88801e-4,
                         'U238': 2.23844e-2,
                         'U234': 4.04814e-6,
                         'U236': 2.23756e-6,
                         'O16':  4.57591e-2},
                'wfFlag': False,
                'density': 10.24,
                'xsT': {293.: '.70c',
                        600.: '.71c',
                        900.: '.72c',
                        1200.: '.73c',
                        2500.: '.74c'}
                },
    # boron carbide control abosrber
    'B4C': {'comp': {'B10':   1.52689E-02,
                     'B11':   6.14591E-02,
                     'C12':   1.91820E-02},
                'wfFlag': False,
                'density': 1.76,
                'xsT': {293.: '.70c',
                        600.: '.71c',
                        900.: '.72c',
                        1200.: '.73c',
                        2500.: '.74c'}
            },
    # Boric acid
    'BOH': {'comp': {'B10':   0.199 * (1 / 7.0),
                     'B11':   0.801 * (1 / 7.0),
                     'H1':    3.0 / 7.0,
                     'O16':   3.0 / 7.0},
                'wfFlag': False,
                'density': 1.435,
                'xsT': {293.: '.70c',
                        600.: '.71c',
                        900.: '.72c',
                        1200.: '.73c',
                        2500.: '.74c'}
            },
    # 00 ppm mod
    '00-lwtr': {'comp': {'H1':  2. / 3.,
                         'O16': 1. / 3.},
                'wfFlag': False,
                'density': 0.8,
                'xsT': {293.: '.70c',
                        600.: '.71c',
                        900.: '.72c',
                        1200.: '.73c',
                        2500.: '.74c'}
                },
    # 1066 ppm mod
    '66-lwtr': {'comp': {'H1':  4.96340E-02,
                         'B10':  8.77976E-06,
                         'B11':  3.53397E-05,
                         'O16':  2.48170E-02},
                'wfFlag': False,
                'density': 0.8,
                'xsT': {293.: '.70c',
                        600.: '.71c',
                        900.: '.72c',
                        1200.: '.73c',
                        2500.: '.74c'}
                },
    # zircalloy 2
    '02-zirc': {'comp': {'Cr50':  3.30121E-06,
                         'Cr52':  6.36606E-05,
                         'Cr53':  7.21860E-06,
                         'Cr54':  1.79686E-06,
                         'Fe54':  8.68307E-06,
                         'Fe56':  1.36306E-04,
                         'Fe57':  3.14789E-06,
                         'Fe58':  4.18926E-07,
                         'Zr90':  2.18865E-02,
                         'Zr91':  4.77292E-03,
                         'Zr92':  7.29551E-03,
                         'Zr94':  7.39335E-03,
                         'Zr96':  1.19110E-03,
                         'Sn112': 4.68066E-06,
                         'Sn114': 3.18478E-06,
                         'Sn115': 1.64064E-06,
                         'Sn116': 7.01616E-05,
                         'Sn117': 3.70592E-05,
                         'Sn118': 1.16872E-04,
                         'Sn119': 4.14504E-05,
                         'Sn120': 1.57212E-04,
                         'Sn122': 2.23417E-05,
                         'Sn124': 2.79392E-05,
                         'Hf174': 3.54138E-09,
                         'Hf176': 1.16423E-07,
                         'Hf177': 4.11686E-07,
                         'Hf178': 6.03806E-07,
                         'Hf179': 3.01460E-07,
                         'Hf180': 7.76449E-07
                         },
                'wfFlag': False,
                'density': 6.56,
                'xsT': {293.: '.70c',
                        600.: '.71c',
                        900.: '.72c',
                        1200.: '.73c',
                        2500.: '.74c'}
                },
    # stainless steel
    'SS': {'comp': {'C12':  3.20895E-04,
                    'Si28':  1.58197E-03,
                    'Si29':  8.03653E-05,
                    'Si30':  5.30394E-05,
                    'P31':   6.99938E-05,
                    'Cr50':  7.64915E-04,
                    'Cr52':  1.47506E-02,
                    'Cr53':  1.67260E-03,
                    'Cr54':  4.16346E-04,
                    'Mn55':  1.75387E-03,
                    'Fe54':  3.44776E-03,
                    'Fe56':  5.41225E-02,
                    'Fe57':  1.24992E-03,
                    'Fe58':  1.66342E-04,
                    'Ni58':  5.30854E-03,
                    'Ni60':  2.04484E-03,
                    'Ni61':  8.88879E-05,
                    'Ni62':  2.83413E-04,
                    'Ni64':  7.21770E-05
                    },
                'wfFlag': False,
                'density': 8.0,
                'xsT': {293.: '.70c',
                        600.: '.71c',
                        900.: '.72c',
                        1200.: '.73c',
                        2500.: '.74c'}
           }
}


def updateAtomicMass(matLib=matLib):
    ''' Update atomic masses and atomic numbers. '''
    for key in matLib.keys():
        zaidStr = str(matLib[key]['zaid'])
        try:
            atM = zaidStr[2:]
            if len(zaidStr) == 4:
                aN = zaidStr[0]
            else:
                aN = zaidStr[:2]
            matLib[key]['M'] = elements[int(aN)][int(atM)].mass
            matLib[key]['A'] = float(elements[int(aN)][int(atM)].number)
        except:
            print("Problem in updating properties for isotope " + str(key))
            if not matLib[key]['M'] or not matLib[key]['A']:
                print("Isotope: " + str(key) + "Supply isotope atomic mass and atomic number manually")
                sys.exit()
            else:
                print("Isotope: " + str(key) + ": Using default values for atomic mass and atomic number")
    return matLib
try:
    from periodictable import *
    matLib = updateAtomicMass(matLib)
except:
    print("WARNING: python package periodictable is not installed")
    print("Continuing with user defined atomic mass definitions")

# Create core plate material
# Create core nozzle material

def generateTempMats(matLib=matLib):
    '''
    Generate <zaid.xsN> materials.  Update matLib.
    '''
    for material, props in mixtureData.iteritems():
        for T, xsN in props['xsT'].iteritems():
            for isoName, atomFrac in props['comp'].iteritems():
                matLib[str(matLib[isoName]['zaid']) + str(xsN)] = matLib[isoName]
                #matLib[isoName]['zaid_xsT-' + str(T)] = str(matLib[isoName]['zaid']) + str(xsN)
    return matLib
matLib = generateTempMats(matLib)


class mixedMat(object):
    '''
    Smeared material class.
    Computes macroscopic cross sections, diffusion coeffs, ect.
    Input number densities in #/b-cm
    Input cross sections in barns
    '''
    Na = 6.02214129e23  # avagadros constant

    def __init__(self, ndDict, wfFlag=False, matLib=matLib):
        # Initilize mixedMat dicts
        self.macroProp = {}
        self.nDdict = {}
        self.wf = {}
        self.af = {}
        self.afF = {}
        # Pull requested isotopes from material library
        try:
            materialData = {k: matLib[k] for k in ndDict.keys()}
        except:
            print("Material __ not found in material library")
        self.microDat = materialData
        # If weight fracs are given, convert to atom frac and proceed as usual
        if wfFlag:
            self.wf = ndDict
            self.nD = self.__wfToAf()
        else:
            self.nD = ndDict
        # Compute properties
        self.__numberDensityTable()
        self.__updateDB()

    def __updateDB(self):
        self.macroProp = {}
        self.__computeAtomicFracs()
        self.__computeMacroXsec()
        self.__computeAvgProps()
        self.__computeDensity()
        self.__afToWf()

    def __numberDensityTable(self):
        '''
        Asscociate each isotope with number density.
        '''
        for i, (material, data) in enumerate(self.microDat.iteritems()):
            self.nDdict[material] = self.nD[material]

    def __computeAtomicFracs(self):
        ''' Compute atomic fractions.  Usefull if material was specified by
        atom density or weight fractions '''
        fissionable_nD_sum = 0
        for i, (material, data) in enumerate(self.microDat.iteritems()):
            self.af[material] = self.nDdict[material] / sum(self.nDdict.values())
            if 'nu' in data.keys():
                if sum(data['nu']) > 0:
                    fissionable_nD_sum += self.nDdict[material]
        for i, (material, data) in enumerate(self.microDat.iteritems()):
            if 'nu' in data.keys():
                try:
                    self.afF[material] = self.nDdict[material] / fissionable_nD_sum
                except:
                    self.afF[material] = 0.0

    def __wfToAf(self):
        '''
        Converts weight fractions to atom fracs
        [g/gtot * mol/g * atoms/mol]  / atoms tot
        assuming gtot = 1
        '''
        atomSum = 0
        for i, (material, data) in enumerate(self.microDat.iteritems()):
            atomSum += self.wf[material] * (1. / self.microDat[material]['M']) * self.Na
        for i, (material, data) in enumerate(self.microDat.iteritems()):
            self.af[material] = self.wf[material] * (1. / self.microDat[material]['M']) * self.Na / atomSum
        return self.af

    def __afToWf(self):
        '''
        Converts atom fractions to weight fractions
        [atoms/atomstot * atomstot * mol/atom * g/mol] / g tot
        assuming atomstot = 1
        '''
        if hasattr(self, 'af'):
            wfSum = 0
            for i, (material, data) in enumerate(self.microDat.iteritems()):
                wfSum += self.af[material] * (1 / self.Na) * self.microDat[material]['M']
            for i, (material, data) in enumerate(self.microDat.iteritems()):
                self.wf[material] = self.af[material] * (1 / self.Na) * self.microDat[material]['M'] / wfSum
        else:
            self.__computeAtomicFracs()
            self.__afToWf()
        return self.wf

    def __computeMacroXsec(self):
        for i, (material, data) in enumerate(self.microDat.iteritems()):
            for key, value in data.iteritems():
                if key in ['sigma_f', 'sigma_a', 'sigma_s', 'sigma_gh']:
                    try:
                        self.macroProp['N' + key] += self.nDdict[material] * value
                    except:
                        self.macroProp['N' + key] = self.nDdict[material] * value
                else:
                    pass

    def __computeAvgProps(self):
        for i, (material, data) in enumerate(self.microDat.iteritems()):
            for key, value in data.iteritems():
                if key in ['M']:
                    try:
                        self.macroProp[key] += self.af[material] * value
                    except:
                        self.macroProp[key] = self.af[material] * value
                elif key in ['nu', 'Chi']:
                    try:
                        self.macroProp[key] += self.afF[material] * value
                    except:
                        self.macroProp[key] = self.afF[material] * value
                elif key in ['A']:
                    try:
                        self.macroProp[key] += self.af[material] * value
                        self.macroProp['mu_bar'] += self.af[material] * (2.0 / (3.0 * value))
                    except:
                        self.macroProp[key] = self.af[material] * value
                        self.macroProp['mu_bar'] = self.af[material] * (2.0 / (3.0 * value))
                else:
                    pass

    def __checkMaterialLib(self):
        '''
        Checks the incomming material libs for missing data.
        '''
        #requiredData = ['sigma_a', 'sigma_s', 'M', 'A']
        pass

    def __isFissile(self):
        ''' extra data for fissile materials '''

        pass

    def __computeDensity(self):
        self.density = sum(self.macroProp['M'] * np.array(self.nDdict.values()) * 1.0e24 /
                           self.Na / np.array(self.af.values())) / len(self.af.values())

    def setDensity(self, density):
        ''' Supply material density in g/cc.  Updates atom densities accordingly. '''
        self.density = density  # g/cc
        # need keep atomic ratios the same, but update atom densities
        # such that the density specified is met
        for i, (material, data) in enumerate(self.microDat.iteritems()):
            # g/cc * #/mol * mol/g
            self.nDdict[material] = density * self.af[material] * self.Na / self.macroProp['M'] / 1.0e24
        self.__updateDB()

    def __add__(self, other):
        '''
        Allows the mixing of predifined mixtures by mass:
        mix 0.2 U238 (by mass) with 0.8 lwtr (by mass) where the
        U238 and lwtr materials were constructed by mixing individual
        isotopes.
        '''
        # add number densities together
        for material, nDensity in other.nDdict.iteritems():
            try:
                self.nDdict[material] += nDensity
            except:
                # We've got a new isotope on our hands!
                self.nDdict[material] = nDensity
                self.microDat[material] = other.microDat[material]
        # update macro property database
        self.__updateDB()
        return self

    def __mul__(self, const):
        '''
        Multiplication by weight fraction.
        '''
        if hasattr(self, 'density'):
            self.setDensity(self.density * const)
        else:
            print("Warning: must specify material density before weighting by mass fraction")
        return self

    def __rmul__(self, const):
        return self.__mul__(const)

for material, props in mixtureData.iteritems():
    for T, xsN in props['xsT'].iteritems():
        compT = {}
        for isoName, atomFrac in props['comp'].iteritems():
            compT[str(matLib[isoName]['zaid']) + str(xsN)] = atomFrac
        mixtureData[material][T] = mixedMat(compT)


if __name__ == "__main__":
    # Testing
    leuMat = mixedMat({'U234': 6.11864e-06,
                       'U235': 0.000718132,
                       'U236': 3.29861e-06,
                       'U238': 0.0221546})
    print leuMat.density

    fuelMat = mixedMat({'U235': 0.005, 'U238': 0.995})
    fuelMat.setDensity(10.2)  # g/cc
    lwtrMat = mixedMat({'O16': 1.0 / 3.0, 'H1': 2.0 / 3.0})
    lwtrMat.setDensity(1.0)  # g/cc
    coreMat = lwtrMat * 0.7 + fuelMat * 0.3
    print coreMat.density
    # use core mat weight fractions to seed a new material and check for
    # consitancy
    checkMat = mixedMat(coreMat.wf, wfFlag=True)
    print coreMat.density
    print coreMat.nDdict
    #
    # Check temperature defined material mixtures

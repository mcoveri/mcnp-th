from setuptools import setup, find_packages

setup(name='mcnpTH',
      version='0.0.1',
      description='MCNP-Fluent driver',
      author='William Gurecky',
      packages=find_packages(),
      install_requires=['numpy>=1.7.0', 'scipy>=0.12.0'],
      package_data={'': ['*.txt']},
      license='BSD',
      author_email='william.gurecky@utexas.edu',
      entry_points={
          'console_scripts': [
              'mcnpTH = coupler.main:main'
          ]
      }
      )
